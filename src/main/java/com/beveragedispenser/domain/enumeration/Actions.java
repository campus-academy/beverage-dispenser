package com.beveragedispenser.domain.enumeration;

/**
 * The Actions enumeration.
 */
public enum Actions {
    BUY,
    RESTOCK,
    OTHER,
}
