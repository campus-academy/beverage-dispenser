package com.beveragedispenser.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * sugared coffee, coffee, chocolate, sugared chocolate
 */
@Schema(description = "sugared coffee, coffee, chocolate, sugared chocolate")
@Entity
@Table(name = "drink")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Drink implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Integer price;

    @ManyToMany
    @JoinTable(
        name = "rel_drink__ingredient",
        joinColumns = @JoinColumn(name = "drink_id"),
        inverseJoinColumns = @JoinColumn(name = "ingredient_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "drinks" }, allowSetters = true)
    private Set<Ingredient> ingredients = new HashSet<>();

    @ManyToMany(mappedBy = "drinks")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "coinAcceptor", "drinks", "infrastructure" }, allowSetters = true)
    private Set<VendingMachine> vendingMachines = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Drink id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Drink name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return this.price;
    }

    public Drink price(Integer price) {
        this.setPrice(price);
        return this;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Set<Ingredient> getIngredients() {
        return this.ingredients;
    }

    public void setIngredients(Set<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public Drink ingredients(Set<Ingredient> ingredients) {
        this.setIngredients(ingredients);
        return this;
    }

    public Drink addIngredient(Ingredient ingredient) {
        this.ingredients.add(ingredient);
        ingredient.getDrinks().add(this);
        return this;
    }

    public Drink removeIngredient(Ingredient ingredient) {
        this.ingredients.remove(ingredient);
        ingredient.getDrinks().remove(this);
        return this;
    }

    public Set<VendingMachine> getVendingMachines() {
        return this.vendingMachines;
    }

    public void setVendingMachines(Set<VendingMachine> vendingMachines) {
        if (this.vendingMachines != null) {
            this.vendingMachines.forEach(i -> i.removeDrink(this));
        }
        if (vendingMachines != null) {
            vendingMachines.forEach(i -> i.addDrink(this));
        }
        this.vendingMachines = vendingMachines;
    }

    public Drink vendingMachines(Set<VendingMachine> vendingMachines) {
        this.setVendingMachines(vendingMachines);
        return this;
    }

    public Drink addVendingMachine(VendingMachine vendingMachine) {
        this.vendingMachines.add(vendingMachine);
        vendingMachine.getDrinks().add(this);
        return this;
    }

    public Drink removeVendingMachine(VendingMachine vendingMachine) {
        this.vendingMachines.remove(vendingMachine);
        vendingMachine.getDrinks().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Drink)) {
            return false;
        }
        return id != null && id.equals(((Drink) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Drink{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", price=" + getPrice() +
            "}";
    }
}
