package com.beveragedispenser.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Sugar, coffee, watter, chocolate, mug, touillette
 */
@Schema(description = "Sugar, coffee, watter, chocolate, mug, touillette")
@Entity
@Table(name = "ingredient")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Ingredient implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Min(value = 0)
    @Max(value = 100)
    @Column(name = "stock")
    private Integer stock;

    @ManyToMany(mappedBy = "ingredients")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "ingredients", "vendingMachines" }, allowSetters = true)
    private Set<Drink> drinks = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Ingredient id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Ingredient name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStock() {
        return this.stock;
    }

    public Ingredient stock(Integer stock) {
        this.setStock(stock);
        return this;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Set<Drink> getDrinks() {
        return this.drinks;
    }

    public void setDrinks(Set<Drink> drinks) {
        if (this.drinks != null) {
            this.drinks.forEach(i -> i.removeIngredient(this));
        }
        if (drinks != null) {
            drinks.forEach(i -> i.addIngredient(this));
        }
        this.drinks = drinks;
    }

    public Ingredient drinks(Set<Drink> drinks) {
        this.setDrinks(drinks);
        return this;
    }

    public Ingredient addDrink(Drink drink) {
        this.drinks.add(drink);
        drink.getIngredients().add(this);
        return this;
    }

    public Ingredient removeDrink(Drink drink) {
        this.drinks.remove(drink);
        drink.getIngredients().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Ingredient)) {
            return false;
        }
        return id != null && id.equals(((Ingredient) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Ingredient{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", stock=" + getStock() +
            "}";
    }
}
