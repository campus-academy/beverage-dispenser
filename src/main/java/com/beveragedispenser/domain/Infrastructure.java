package com.beveragedispenser.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Infrastructure.
 */
@Entity
@Table(name = "infrastructure")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Infrastructure implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "infrastructure")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "coinAcceptor", "drinks", "infrastructure" }, allowSetters = true)
    private Set<VendingMachine> vendingMachines = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Infrastructure id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Infrastructure name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<VendingMachine> getVendingMachines() {
        return this.vendingMachines;
    }

    public void setVendingMachines(Set<VendingMachine> vendingMachines) {
        if (this.vendingMachines != null) {
            this.vendingMachines.forEach(i -> i.setInfrastructure(null));
        }
        if (vendingMachines != null) {
            vendingMachines.forEach(i -> i.setInfrastructure(this));
        }
        this.vendingMachines = vendingMachines;
    }

    public Infrastructure vendingMachines(Set<VendingMachine> vendingMachines) {
        this.setVendingMachines(vendingMachines);
        return this;
    }

    public Infrastructure addVendingMachine(VendingMachine vendingMachine) {
        this.vendingMachines.add(vendingMachine);
        vendingMachine.setInfrastructure(this);
        return this;
    }

    public Infrastructure removeVendingMachine(VendingMachine vendingMachine) {
        this.vendingMachines.remove(vendingMachine);
        vendingMachine.setInfrastructure(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Infrastructure)) {
            return false;
        }
        return id != null && id.equals(((Infrastructure) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Infrastructure{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
