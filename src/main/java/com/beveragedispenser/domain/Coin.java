package com.beveragedispenser.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Coin.
 */
@Entity
@Table(name = "coin")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Coin implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "value")
    private Integer value;

    @ManyToMany(mappedBy = "coins")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "coins", "vendingMachine" }, allowSetters = true)
    private Set<CoinAcceptor> coinAcceptors = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Coin id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Coin name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return this.value;
    }

    public Coin value(Integer value) {
        this.setValue(value);
        return this;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Set<CoinAcceptor> getCoinAcceptors() {
        return this.coinAcceptors;
    }

    public void setCoinAcceptors(Set<CoinAcceptor> coinAcceptors) {
        if (this.coinAcceptors != null) {
            this.coinAcceptors.forEach(i -> i.removeCoin(this));
        }
        if (coinAcceptors != null) {
            coinAcceptors.forEach(i -> i.addCoin(this));
        }
        this.coinAcceptors = coinAcceptors;
    }

    public Coin coinAcceptors(Set<CoinAcceptor> coinAcceptors) {
        this.setCoinAcceptors(coinAcceptors);
        return this;
    }

    public Coin addCoinAcceptor(CoinAcceptor coinAcceptor) {
        this.coinAcceptors.add(coinAcceptor);
        coinAcceptor.getCoins().add(this);
        return this;
    }

    public Coin removeCoinAcceptor(CoinAcceptor coinAcceptor) {
        this.coinAcceptors.remove(coinAcceptor);
        coinAcceptor.getCoins().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Coin)) {
            return false;
        }
        return id != null && id.equals(((Coin) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Coin{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", value=" + getValue() +
            "}";
    }
}
