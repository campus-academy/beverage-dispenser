package com.beveragedispenser.domain;

import com.beveragedispenser.domain.enumeration.Actions;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A History.
 */
@Entity
@Table(name = "history")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class History implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "machine_id")
    private Integer machineId;

    @Column(name = "total")
    private Integer total;

    @Column(name = "drink_id")
    private Integer drinkId;

    @Column(name = "ingredient_id")
    private Integer ingredientId;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "action")
    private Actions action;

    @JsonIgnoreProperties(value = { "vendingMachines" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Infrastructure infrastructureId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public History id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMachineId() {
        return this.machineId;
    }

    public History machineId(Integer machineId) {
        this.setMachineId(machineId);
        return this;
    }

    public void setMachineId(Integer machineId) {
        this.machineId = machineId;
    }

    public Integer getTotal() {
        return this.total;
    }

    public History total(Integer total) {
        this.setTotal(total);
        return this;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getDrinkId() {
        return this.drinkId;
    }

    public History drinkId(Integer drinkId) {
        this.setDrinkId(drinkId);
        return this;
    }

    public void setDrinkId(Integer drinkId) {
        this.drinkId = drinkId;
    }

    public Integer getIngredientId() {
        return this.ingredientId;
    }

    public History ingredientId(Integer ingredientId) {
        this.setIngredientId(ingredientId);
        return this;
    }

    public void setIngredientId(Integer ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getDescription() {
        return this.description;
    }

    public History description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Actions getAction() {
        return this.action;
    }

    public History action(Actions action) {
        this.setAction(action);
        return this;
    }

    public void setAction(Actions action) {
        this.action = action;
    }

    public Infrastructure getInfrastructureId() {
        return this.infrastructureId;
    }

    public void setInfrastructureId(Infrastructure infrastructure) {
        this.infrastructureId = infrastructure;
    }

    public History infrastructureId(Infrastructure infrastructure) {
        this.setInfrastructureId(infrastructure);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof History)) {
            return false;
        }
        return id != null && id.equals(((History) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "History{" +
            "id=" + getId() +
            ", machineId=" + getMachineId() +
            ", total=" + getTotal() +
            ", drinkId=" + getDrinkId() +
            ", ingredientId=" + getIngredientId() +
            ", description='" + getDescription() + "'" +
            ", action='" + getAction() + "'" +
            "}";
    }
}
