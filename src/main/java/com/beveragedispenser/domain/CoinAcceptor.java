package com.beveragedispenser.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A CoinAcceptor.
 */
@Entity
@Table(name = "coin_acceptor")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CoinAcceptor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "requested_currency")
    private Integer requestedCurrency;

    @ManyToMany
    @JoinTable(
        name = "rel_coin_acceptor__coin",
        joinColumns = @JoinColumn(name = "coin_acceptor_id"),
        inverseJoinColumns = @JoinColumn(name = "coin_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "coinAcceptors" }, allowSetters = true)
    private Set<Coin> coins = new HashSet<>();

    @JsonIgnoreProperties(value = { "coinAcceptor", "drinks", "infrastructure" }, allowSetters = true)
    @OneToOne(mappedBy = "coinAcceptor")
    private VendingMachine vendingMachine;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CoinAcceptor id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public CoinAcceptor name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRequestedCurrency() {
        return this.requestedCurrency;
    }

    public CoinAcceptor requestedCurrency(Integer requestedCurrency) {
        this.setRequestedCurrency(requestedCurrency);
        return this;
    }

    public void setRequestedCurrency(Integer requestedCurrency) {
        this.requestedCurrency = requestedCurrency;
    }

    public Set<Coin> getCoins() {
        return this.coins;
    }

    public void setCoins(Set<Coin> coins) {
        this.coins = coins;
    }

    public CoinAcceptor coins(Set<Coin> coins) {
        this.setCoins(coins);
        return this;
    }

    public CoinAcceptor addCoin(Coin coin) {
        this.coins.add(coin);
        coin.getCoinAcceptors().add(this);
        return this;
    }

    public CoinAcceptor removeCoin(Coin coin) {
        this.coins.remove(coin);
        coin.getCoinAcceptors().remove(this);
        return this;
    }

    public VendingMachine getVendingMachine() {
        return this.vendingMachine;
    }

    public void setVendingMachine(VendingMachine vendingMachine) {
        if (this.vendingMachine != null) {
            this.vendingMachine.setCoinAcceptor(null);
        }
        if (vendingMachine != null) {
            vendingMachine.setCoinAcceptor(this);
        }
        this.vendingMachine = vendingMachine;
    }

    public CoinAcceptor vendingMachine(VendingMachine vendingMachine) {
        this.setVendingMachine(vendingMachine);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CoinAcceptor)) {
            return false;
        }
        return id != null && id.equals(((CoinAcceptor) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CoinAcceptor{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", requestedCurrency=" + getRequestedCurrency() +
            "}";
    }
}
