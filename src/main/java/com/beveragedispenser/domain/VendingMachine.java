package com.beveragedispenser.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * not an ignored comment
 */
@Schema(description = "not an ignored comment")
@Entity
@Table(name = "vending_machine")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class VendingMachine implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "serial_number")
    private String serialNumber;

    @Column(name = "brand")
    private String brand;

    @Column(name = "model")
    private String model;

    @JsonIgnoreProperties(value = { "coins", "vendingMachine" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private CoinAcceptor coinAcceptor;

    @ManyToMany
    @JoinTable(
        name = "rel_vending_machine__drink",
        joinColumns = @JoinColumn(name = "vending_machine_id"),
        inverseJoinColumns = @JoinColumn(name = "drink_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "ingredients", "vendingMachines" }, allowSetters = true)
    private Set<Drink> drinks = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "vendingMachines" }, allowSetters = true)
    private Infrastructure infrastructure;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public VendingMachine id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public VendingMachine name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerialNumber() {
        return this.serialNumber;
    }

    public VendingMachine serialNumber(String serialNumber) {
        this.setSerialNumber(serialNumber);
        return this;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getBrand() {
        return this.brand;
    }

    public VendingMachine brand(String brand) {
        this.setBrand(brand);
        return this;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return this.model;
    }

    public VendingMachine model(String model) {
        this.setModel(model);
        return this;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public CoinAcceptor getCoinAcceptor() {
        return this.coinAcceptor;
    }

    public void setCoinAcceptor(CoinAcceptor coinAcceptor) {
        this.coinAcceptor = coinAcceptor;
    }

    public VendingMachine coinAcceptor(CoinAcceptor coinAcceptor) {
        this.setCoinAcceptor(coinAcceptor);
        return this;
    }

    public Set<Drink> getDrinks() {
        return this.drinks;
    }

    public void setDrinks(Set<Drink> drinks) {
        this.drinks = drinks;
    }

    public VendingMachine drinks(Set<Drink> drinks) {
        this.setDrinks(drinks);
        return this;
    }

    public VendingMachine addDrink(Drink drink) {
        this.drinks.add(drink);
        drink.getVendingMachines().add(this);
        return this;
    }

    public VendingMachine removeDrink(Drink drink) {
        this.drinks.remove(drink);
        drink.getVendingMachines().remove(this);
        return this;
    }

    public Infrastructure getInfrastructure() {
        return this.infrastructure;
    }

    public void setInfrastructure(Infrastructure infrastructure) {
        this.infrastructure = infrastructure;
    }

    public VendingMachine infrastructure(Infrastructure infrastructure) {
        this.setInfrastructure(infrastructure);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VendingMachine)) {
            return false;
        }
        return id != null && id.equals(((VendingMachine) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VendingMachine{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", serialNumber='" + getSerialNumber() + "'" +
            ", brand='" + getBrand() + "'" +
            ", model='" + getModel() + "'" +
            "}";
    }
}
