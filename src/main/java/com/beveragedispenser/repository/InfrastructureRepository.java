package com.beveragedispenser.repository;

import com.beveragedispenser.domain.Infrastructure;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Infrastructure entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InfrastructureRepository extends JpaRepository<Infrastructure, Long> {}
