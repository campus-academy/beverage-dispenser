package com.beveragedispenser.repository;

import com.beveragedispenser.domain.VendingMachine;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the VendingMachine entity.
 */
@Repository
public interface VendingMachineRepository extends JpaRepository<VendingMachine, Long> {
    @Query(
        value = "select distinct vendingMachine from VendingMachine vendingMachine left join fetch vendingMachine.drinks",
        countQuery = "select count(distinct vendingMachine) from VendingMachine vendingMachine"
    )
    Page<VendingMachine> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct vendingMachine from VendingMachine vendingMachine left join fetch vendingMachine.drinks")
    List<VendingMachine> findAllWithEagerRelationships();

    @Query("select vendingMachine from VendingMachine vendingMachine left join fetch vendingMachine.drinks where vendingMachine.id =:id")
    Optional<VendingMachine> findOneWithEagerRelationships(@Param("id") Long id);
}
