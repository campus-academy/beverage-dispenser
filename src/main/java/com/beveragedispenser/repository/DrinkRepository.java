package com.beveragedispenser.repository;

import com.beveragedispenser.domain.Drink;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Drink entity.
 */
@Repository
public interface DrinkRepository extends JpaRepository<Drink, Long> {
    @Query(
        value = "select distinct drink from Drink drink left join fetch drink.ingredients",
        countQuery = "select count(distinct drink) from Drink drink"
    )
    Page<Drink> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct drink from Drink drink left join fetch drink.ingredients")
    List<Drink> findAllWithEagerRelationships();

    @Query("select drink from Drink drink left join fetch drink.ingredients where drink.id =:id")
    Optional<Drink> findOneWithEagerRelationships(@Param("id") Long id);
}
