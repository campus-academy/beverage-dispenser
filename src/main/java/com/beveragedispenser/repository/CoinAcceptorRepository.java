package com.beveragedispenser.repository;

import com.beveragedispenser.domain.CoinAcceptor;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the CoinAcceptor entity.
 */
@Repository
public interface CoinAcceptorRepository extends JpaRepository<CoinAcceptor, Long> {
    @Query(
        value = "select distinct coinAcceptor from CoinAcceptor coinAcceptor left join fetch coinAcceptor.coins",
        countQuery = "select count(distinct coinAcceptor) from CoinAcceptor coinAcceptor"
    )
    Page<CoinAcceptor> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct coinAcceptor from CoinAcceptor coinAcceptor left join fetch coinAcceptor.coins")
    List<CoinAcceptor> findAllWithEagerRelationships();

    @Query("select coinAcceptor from CoinAcceptor coinAcceptor left join fetch coinAcceptor.coins where coinAcceptor.id =:id")
    Optional<CoinAcceptor> findOneWithEagerRelationships(@Param("id") Long id);
}
