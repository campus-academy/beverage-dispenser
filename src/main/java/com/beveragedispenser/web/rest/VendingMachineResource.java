package com.beveragedispenser.web.rest;

import com.beveragedispenser.domain.VendingMachine;
import com.beveragedispenser.repository.VendingMachineRepository;
import com.beveragedispenser.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.beveragedispenser.domain.VendingMachine}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class VendingMachineResource {

    private final Logger log = LoggerFactory.getLogger(VendingMachineResource.class);

    private static final String ENTITY_NAME = "vendingMachine";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VendingMachineRepository vendingMachineRepository;

    public VendingMachineResource(VendingMachineRepository vendingMachineRepository) {
        this.vendingMachineRepository = vendingMachineRepository;
    }

    /**
     * {@code POST  /vending-machines} : Create a new vendingMachine.
     *
     * @param vendingMachine the vendingMachine to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new vendingMachine, or with status {@code 400 (Bad Request)} if the vendingMachine has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/vending-machines")
    public ResponseEntity<VendingMachine> createVendingMachine(@RequestBody VendingMachine vendingMachine) throws URISyntaxException {
        log.debug("REST request to save VendingMachine : {}", vendingMachine);
        if (vendingMachine.getId() != null) {
            throw new BadRequestAlertException("A new vendingMachine cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VendingMachine result = vendingMachineRepository.save(vendingMachine);
        return ResponseEntity
            .created(new URI("/api/vending-machines/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /vending-machines/:id} : Updates an existing vendingMachine.
     *
     * @param id the id of the vendingMachine to save.
     * @param vendingMachine the vendingMachine to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vendingMachine,
     * or with status {@code 400 (Bad Request)} if the vendingMachine is not valid,
     * or with status {@code 500 (Internal Server Error)} if the vendingMachine couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/vending-machines/{id}")
    public ResponseEntity<VendingMachine> updateVendingMachine(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody VendingMachine vendingMachine
    ) throws URISyntaxException {
        log.debug("REST request to update VendingMachine : {}, {}", id, vendingMachine);
        if (vendingMachine.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, vendingMachine.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!vendingMachineRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        VendingMachine result = vendingMachineRepository.save(vendingMachine);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, vendingMachine.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /vending-machines/:id} : Partial updates given fields of an existing vendingMachine, field will ignore if it is null
     *
     * @param id the id of the vendingMachine to save.
     * @param vendingMachine the vendingMachine to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vendingMachine,
     * or with status {@code 400 (Bad Request)} if the vendingMachine is not valid,
     * or with status {@code 404 (Not Found)} if the vendingMachine is not found,
     * or with status {@code 500 (Internal Server Error)} if the vendingMachine couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/vending-machines/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<VendingMachine> partialUpdateVendingMachine(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody VendingMachine vendingMachine
    ) throws URISyntaxException {
        log.debug("REST request to partial update VendingMachine partially : {}, {}", id, vendingMachine);
        if (vendingMachine.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, vendingMachine.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!vendingMachineRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<VendingMachine> result = vendingMachineRepository
            .findById(vendingMachine.getId())
            .map(existingVendingMachine -> {
                if (vendingMachine.getName() != null) {
                    existingVendingMachine.setName(vendingMachine.getName());
                }
                if (vendingMachine.getSerialNumber() != null) {
                    existingVendingMachine.setSerialNumber(vendingMachine.getSerialNumber());
                }
                if (vendingMachine.getBrand() != null) {
                    existingVendingMachine.setBrand(vendingMachine.getBrand());
                }
                if (vendingMachine.getModel() != null) {
                    existingVendingMachine.setModel(vendingMachine.getModel());
                }

                return existingVendingMachine;
            })
            .map(vendingMachineRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, vendingMachine.getId().toString())
        );
    }

    /**
     * {@code GET  /vending-machines} : get all the vendingMachines.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of vendingMachines in body.
     */
    @GetMapping("/vending-machines")
    public List<VendingMachine> getAllVendingMachines(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all VendingMachines");
        return vendingMachineRepository.findAllWithEagerRelationships();
    }

    /**
     * {@code GET  /vending-machines/:id} : get the "id" vendingMachine.
     *
     * @param id the id of the vendingMachine to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the vendingMachine, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/vending-machines/{id}")
    public ResponseEntity<VendingMachine> getVendingMachine(@PathVariable Long id) {
        log.debug("REST request to get VendingMachine : {}", id);
        Optional<VendingMachine> vendingMachine = vendingMachineRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(vendingMachine);
    }

    /**
     * {@code DELETE  /vending-machines/:id} : delete the "id" vendingMachine.
     *
     * @param id the id of the vendingMachine to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/vending-machines/{id}")
    public ResponseEntity<Void> deleteVendingMachine(@PathVariable Long id) {
        log.debug("REST request to delete VendingMachine : {}", id);
        vendingMachineRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
