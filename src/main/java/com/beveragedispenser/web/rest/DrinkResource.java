package com.beveragedispenser.web.rest;

import com.beveragedispenser.domain.Drink;
import com.beveragedispenser.repository.DrinkRepository;
import com.beveragedispenser.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import net.logstash.logback.encoder.com.lmax.disruptor.SleepingWaitStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.beveragedispenser.domain.Drink}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class DrinkResource {

    private final Logger log = LoggerFactory.getLogger(DrinkResource.class);

    private static final String ENTITY_NAME = "drink";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DrinkRepository drinkRepository;

    public DrinkResource(DrinkRepository drinkRepository) {
        this.drinkRepository = drinkRepository;
    }

    /**
     * {@code POST  /drinks} : Create a new drink.
     *
     * @param drink the drink to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new drink, or with status {@code 400 (Bad Request)} if the drink has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/drinks")
    public ResponseEntity<Drink> createDrink(@RequestBody Drink drink) throws URISyntaxException {
        log.debug("REST request to save Drink : {}", drink);
        if (drink.getId() != null) {
            throw new BadRequestAlertException("A new drink cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Drink result = drinkRepository.save(drink);
        return ResponseEntity
            .created(new URI("/api/drinks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /drinks/:id} : Updates an existing drink.
     *
     * @param id the id of the drink to save.
     * @param drink the drink to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated drink,
     * or with status {@code 400 (Bad Request)} if the drink is not valid,
     * or with status {@code 500 (Internal Server Error)} if the drink couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/drinks/{id}")
    public ResponseEntity<Drink> updateDrink(@PathVariable(value = "id", required = false) final Long id, @RequestBody Drink drink)
        throws URISyntaxException {
        log.debug("REST request to update Drink : {}, {}", id, drink);
        if (drink.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, drink.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!drinkRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Drink result = drinkRepository.save(drink);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, drink.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /drinks/:id} : Partial updates given fields of an existing drink, field will ignore if it is null
     *
     * @param id the id of the drink to save.
     * @param drink the drink to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated drink,
     * or with status {@code 400 (Bad Request)} if the drink is not valid,
     * or with status {@code 404 (Not Found)} if the drink is not found,
     * or with status {@code 500 (Internal Server Error)} if the drink couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/drinks/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Drink> partialUpdateDrink(@PathVariable(value = "id", required = false) final Long id, @RequestBody Drink drink)
        throws URISyntaxException {
        log.debug("REST request to partial update Drink partially : {}, {}", id, drink);
        if (drink.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, drink.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!drinkRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Drink> result = drinkRepository
            .findById(drink.getId())
            .map(existingDrink -> {
                if (drink.getName() != null) {
                    existingDrink.setName(drink.getName());
                }
                if (drink.getPrice() != null) {
                    existingDrink.setPrice(drink.getPrice());
                }

                return existingDrink;
            })
            .map(drinkRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, drink.getId().toString())
        );
    }

    /**
     * {@code GET  /drinks} : get all the drinks.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of drinks in body.
     */
    @GetMapping("/drinks")
    public List<Drink> getAllDrinks(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Drinks");
        return drinkRepository.findAllWithEagerRelationships();
    }

    /**
     * {@code GET  /drinks/:id} : get the "id" drink.
     *
     * @param id the id of the drink to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the drink, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/drinks/{id}")
    public ResponseEntity<Drink> getDrink(@PathVariable Long id) {
        log.debug("REST request to get Drink : {}", id);
        Optional<Drink> drink = drinkRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(drink);
    }

    /**
     * {@code PUT  /drinks/make/:id} : make the "id" drink.
     *
     * @param id the id of the drink to build.
     * @throws InterruptedException
     */
    @PutMapping("/drinks/make/{id}")
    public void makeDrink(@PathVariable Long id) throws InterruptedException {
        log.debug("REST request to make Drink : {}", id);

        drinkRepository
            .findOneWithEagerRelationships(id)
            .ifPresent(drink -> {
                drink
                    .getIngredients()
                    .forEach(ingredient -> {
                        int currentStock = ingredient.getStock();

                        if (currentStock <= 0) {
                            String error = "Plus de ".concat(ingredient.getName());
                            throw new BadRequestAlertException(error, ENTITY_NAME, "ingredientNotFound");
                        }
                    });

                drink
                    .getIngredients()
                    .forEach(ingredient -> {
                        int currentStock = ingredient.getStock();
                        ingredient.setStock(currentStock - 1);
                    });
            });
    }

    /**
     * {@code DELETE  /drinks/:id} : delete the "id" drink.
     *
     * @param id the id of the drink to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/drinks/{id}")
    public ResponseEntity<Void> deleteDrink(@PathVariable Long id) {
        log.debug("REST request to delete Drink : {}", id);
        drinkRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
