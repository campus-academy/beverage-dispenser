package com.beveragedispenser.web.rest;

import com.beveragedispenser.domain.Coin;
import com.beveragedispenser.domain.CoinAcceptor;
import com.beveragedispenser.repository.CoinAcceptorRepository;
import com.beveragedispenser.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.beveragedispenser.domain.CoinAcceptor}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CoinAcceptorResource {

    private final Logger log = LoggerFactory.getLogger(CoinAcceptorResource.class);

    private static final String ENTITY_NAME = "coinAcceptor";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CoinAcceptorRepository coinAcceptorRepository;

    public CoinAcceptorResource(CoinAcceptorRepository coinAcceptorRepository) {
        this.coinAcceptorRepository = coinAcceptorRepository;
    }

    /**
     * {@code POST  /coin-acceptors} : Create a new coinAcceptor.
     *
     * @param coinAcceptor the coinAcceptor to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new coinAcceptor, or with status {@code 400 (Bad Request)} if the coinAcceptor has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/coin-acceptors")
    public ResponseEntity<CoinAcceptor> createCoinAcceptor(@RequestBody CoinAcceptor coinAcceptor) throws URISyntaxException {
        log.debug("REST request to save CoinAcceptor : {}", coinAcceptor);
        if (coinAcceptor.getId() != null) {
            throw new BadRequestAlertException("A new coinAcceptor cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CoinAcceptor result = coinAcceptorRepository.save(coinAcceptor);
        return ResponseEntity
            .created(new URI("/api/coin-acceptors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /coin-acceptors/:id} : Updates an existing coinAcceptor.
     *
     * @param id the id of the coinAcceptor to save.
     * @param coinAcceptor the coinAcceptor to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coinAcceptor,
     * or with status {@code 400 (Bad Request)} if the coinAcceptor is not valid,
     * or with status {@code 500 (Internal Server Error)} if the coinAcceptor couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/coin-acceptors/{id}")
    public ResponseEntity<CoinAcceptor> updateCoinAcceptor(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CoinAcceptor coinAcceptor
    ) throws URISyntaxException {
        log.debug("REST request to update CoinAcceptor : {}, {}", id, coinAcceptor);
        if (coinAcceptor.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, coinAcceptor.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!coinAcceptorRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CoinAcceptor result = coinAcceptorRepository.save(coinAcceptor);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, coinAcceptor.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /coin-acceptors/:id} : Partial updates given fields of an existing coinAcceptor, field will ignore if it is null
     *
     * @param id the id of the coinAcceptor to save.
     * @param coinAcceptor the coinAcceptor to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coinAcceptor,
     * or with status {@code 400 (Bad Request)} if the coinAcceptor is not valid,
     * or with status {@code 404 (Not Found)} if the coinAcceptor is not found,
     * or with status {@code 500 (Internal Server Error)} if the coinAcceptor couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/coin-acceptors/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CoinAcceptor> partialUpdateCoinAcceptor(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CoinAcceptor coinAcceptor
    ) throws URISyntaxException {
        log.debug("REST request to partial update CoinAcceptor partially : {}, {}", id, coinAcceptor);
        if (coinAcceptor.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, coinAcceptor.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!coinAcceptorRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CoinAcceptor> result = coinAcceptorRepository
            .findById(coinAcceptor.getId())
            .map(existingCoinAcceptor -> {
                if (coinAcceptor.getName() != null) {
                    existingCoinAcceptor.setName(coinAcceptor.getName());
                }
                if (coinAcceptor.getRequestedCurrency() != null) {
                    existingCoinAcceptor.setRequestedCurrency(coinAcceptor.getRequestedCurrency());
                }

                return existingCoinAcceptor;
            })
            .map(coinAcceptorRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, coinAcceptor.getId().toString())
        );
    }

    /**
     * {@code GET  /coin-acceptors} : get all the coinAcceptors.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of coinAcceptors in body.
     */
    @GetMapping("/coin-acceptors")
    public List<CoinAcceptor> getAllCoinAcceptors(
        @RequestParam(required = false) String filter,
        @RequestParam(required = false, defaultValue = "false") boolean eagerload
    ) {
        if ("vendingmachine-is-null".equals(filter)) {
            log.debug("REST request to get all CoinAcceptors where vendingMachine is null");
            return StreamSupport
                    .stream(coinAcceptorRepository.findAll().spliterator(), false)
                    .filter(coinAcceptor -> coinAcceptor.getVendingMachine() == null)
                    .collect(Collectors.toList());
        }
        log.debug("REST request to get all CoinAcceptors");
        return coinAcceptorRepository.findAllWithEagerRelationships();
    }

    /**
     * {@code GET  /coin-acceptors/:id} : get the "id" coinAcceptor.
     *
     * @param id the id of the coinAcceptor to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the coinAcceptor, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/coin-acceptors/{id}")
    public ResponseEntity<CoinAcceptor> getCoinAcceptor(@PathVariable Long id) {
        log.debug("REST request to get CoinAcceptor : {}", id);
        Optional<CoinAcceptor> coinAcceptor = coinAcceptorRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(coinAcceptor);
    }
    
    /**
     * {@code GET  /coin-acceptors/:id/coin/:coinId} : get the "id" coinAcceptor.
     *
     * @param id the id of the coinAcceptor to retrieve.
     * @param coinId the id of the coin.
     */
    @GetMapping("/coin-acceptors/{id}/coin/{coinId}")
    public void getCoinAcceptorIsValid(@PathVariable Long id, @PathVariable Long coinId) {
        coinAcceptorRepository.findOneWithEagerRelationships(id).ifPresent(coinAcceptor -> {
            // Initialise [0];
            int[] acceptedCoin = new int[1];

            coinAcceptor.getCoins().forEach((coin) -> {
                if (coin.getId() == coinId) {
                    acceptedCoin[0] = 1;
                }
            });

            if (acceptedCoin[0] == 0) {
                throw new BadRequestAlertException("Pièce non acceptée", ENTITY_NAME,
                        "invalidCoin");
            }
        });
    }

    /**
     * {@code DELETE  /coin-acceptors/:id} : delete the "id" coinAcceptor.
     *
     * @param id the id of the coinAcceptor to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/coin-acceptors/{id}")
    public ResponseEntity<Void> deleteCoinAcceptor(@PathVariable Long id) {
        log.debug("REST request to delete CoinAcceptor : {}", id);
        coinAcceptorRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
