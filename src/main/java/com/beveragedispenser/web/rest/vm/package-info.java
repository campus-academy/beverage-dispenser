/**
 * View Models used by Spring MVC REST controllers.
 */
package com.beveragedispenser.web.rest.vm;
