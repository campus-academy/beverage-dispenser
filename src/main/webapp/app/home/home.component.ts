import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/auth/account.model';
import { CoinAcceptorService } from '../entities/coin-acceptor/service/coin-acceptor.service';
import { DrinkService } from '../entities/drink/service/drink.service';
import { Drink, IDrink } from '../entities/drink/drink.model';
import { CoinService } from '../entities/coin/service/coin.service';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  drinks: any;
  coins: any;
  selectedDrink: any;

  private readonly destroy$ = new Subject<void>();

  constructor(
    private accountService: AccountService,
    private router: Router,
    private coinAcceptor: CoinAcceptorService,
    private drinkService: DrinkService,
    private coinService: CoinService
  ) {}

  ngOnInit(): void {
    this.accountService
      .getAuthenticationState()
      .pipe(takeUntil(this.destroy$))
      .subscribe(account => (this.account = account));

    this.drinkService.query().subscribe(result => {
      console.log(result);
      this.drinks = result.body as IDrink[];
    });

    this.coinService.query().subscribe(result => {
      console.log(result);
      this.coins = result.body as IDrink[];
    });
  }

  login(): void {
    this.router.navigate(['/login']);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  selectDrink(id: number): void {
    this.selectedDrink = id;
  }

  addCoin(id: number): void {
    this.coinAcceptor
      .sumChecker(id)
      .then(result => {
        alert('Pièce est acceptée');
      })
      .catch(error => alert("La pièce n'est pas acceptée"));
  }

  makeDrink(): void {
    if (this.selectedDrink !== null) {
      this.drinkService.makeDrink(this.selectedDrink);
    } else {
      alert('sélectionner une boisson avant');
    }
  }
}
