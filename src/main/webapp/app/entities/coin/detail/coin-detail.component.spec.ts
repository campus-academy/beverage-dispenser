import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CoinDetailComponent } from './coin-detail.component';

describe('Coin Management Detail Component', () => {
  let comp: CoinDetailComponent;
  let fixture: ComponentFixture<CoinDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CoinDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ coin: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CoinDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CoinDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load coin on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.coin).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
