import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CoinComponent } from './list/coin.component';
import { CoinDetailComponent } from './detail/coin-detail.component';
import { CoinUpdateComponent } from './update/coin-update.component';
import { CoinDeleteDialogComponent } from './delete/coin-delete-dialog.component';
import { CoinRoutingModule } from './route/coin-routing.module';

@NgModule({
  imports: [SharedModule, CoinRoutingModule],
  declarations: [CoinComponent, CoinDetailComponent, CoinUpdateComponent, CoinDeleteDialogComponent],
  entryComponents: [CoinDeleteDialogComponent],
})
export class CoinModule {}
