import { ICoinAcceptor } from 'app/entities/coin-acceptor/coin-acceptor.model';

export interface ICoin {
  id?: number;
  name?: string | null;
  value?: number | null;
  coinAcceptors?: ICoinAcceptor[] | null;
}

export class Coin implements ICoin {
  constructor(
    public id?: number,
    public name?: string | null,
    public value?: number | null,
    public coinAcceptors?: ICoinAcceptor[] | null
  ) {}
}

export function getCoinIdentifier(coin: ICoin): number | undefined {
  return coin.id;
}
