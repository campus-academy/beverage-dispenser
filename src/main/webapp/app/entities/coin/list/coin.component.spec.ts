import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { CoinService } from '../service/coin.service';

import { CoinComponent } from './coin.component';

describe('Coin Management Component', () => {
  let comp: CoinComponent;
  let fixture: ComponentFixture<CoinComponent>;
  let service: CoinService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CoinComponent],
    })
      .overrideTemplate(CoinComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CoinComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(CoinService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.coins?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
