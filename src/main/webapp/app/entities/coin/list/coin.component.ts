import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICoin } from '../coin.model';
import { CoinService } from '../service/coin.service';
import { CoinDeleteDialogComponent } from '../delete/coin-delete-dialog.component';

@Component({
  selector: 'jhi-coin',
  templateUrl: './coin.component.html',
})
export class CoinComponent implements OnInit {
  coins?: ICoin[];
  isLoading = false;

  constructor(protected coinService: CoinService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.coinService.query().subscribe({
      next: (res: HttpResponse<ICoin[]>) => {
        this.isLoading = false;
        this.coins = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ICoin): number {
    return item.id!;
  }

  delete(coin: ICoin): void {
    const modalRef = this.modalService.open(CoinDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.coin = coin;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
