import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICoin } from '../coin.model';
import { CoinService } from '../service/coin.service';

@Component({
  templateUrl: './coin-delete-dialog.component.html',
})
export class CoinDeleteDialogComponent {
  coin?: ICoin;

  constructor(protected coinService: CoinService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.coinService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
