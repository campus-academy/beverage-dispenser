import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ICoin, Coin } from '../coin.model';
import { CoinService } from '../service/coin.service';

@Component({
  selector: 'jhi-coin-update',
  templateUrl: './coin-update.component.html',
})
export class CoinUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    value: [],
  });

  constructor(protected coinService: CoinService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ coin }) => {
      this.updateForm(coin);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const coin = this.createFromForm();
    if (coin.id !== undefined) {
      this.subscribeToSaveResponse(this.coinService.update(coin));
    } else {
      this.subscribeToSaveResponse(this.coinService.create(coin));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICoin>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(coin: ICoin): void {
    this.editForm.patchValue({
      id: coin.id,
      name: coin.name,
      value: coin.value,
    });
  }

  protected createFromForm(): ICoin {
    return {
      ...new Coin(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      value: this.editForm.get(['value'])!.value,
    };
  }
}
