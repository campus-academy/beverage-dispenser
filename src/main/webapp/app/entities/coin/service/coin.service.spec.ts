import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICoin, Coin } from '../coin.model';

import { CoinService } from './coin.service';

describe('Coin Service', () => {
  let service: CoinService;
  let httpMock: HttpTestingController;
  let elemDefault: ICoin;
  let expectedResult: ICoin | ICoin[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CoinService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      value: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Coin', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Coin()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Coin', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          value: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Coin', () => {
      const patchObject = Object.assign(
        {
          name: 'BBBBBB',
          value: 1,
        },
        new Coin()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Coin', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          value: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Coin', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCoinToCollectionIfMissing', () => {
      it('should add a Coin to an empty array', () => {
        const coin: ICoin = { id: 123 };
        expectedResult = service.addCoinToCollectionIfMissing([], coin);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(coin);
      });

      it('should not add a Coin to an array that contains it', () => {
        const coin: ICoin = { id: 123 };
        const coinCollection: ICoin[] = [
          {
            ...coin,
          },
          { id: 456 },
        ];
        expectedResult = service.addCoinToCollectionIfMissing(coinCollection, coin);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Coin to an array that doesn't contain it", () => {
        const coin: ICoin = { id: 123 };
        const coinCollection: ICoin[] = [{ id: 456 }];
        expectedResult = service.addCoinToCollectionIfMissing(coinCollection, coin);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(coin);
      });

      it('should add only unique Coin to an array', () => {
        const coinArray: ICoin[] = [{ id: 123 }, { id: 456 }, { id: 60143 }];
        const coinCollection: ICoin[] = [{ id: 123 }];
        expectedResult = service.addCoinToCollectionIfMissing(coinCollection, ...coinArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const coin: ICoin = { id: 123 };
        const coin2: ICoin = { id: 456 };
        expectedResult = service.addCoinToCollectionIfMissing([], coin, coin2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(coin);
        expect(expectedResult).toContain(coin2);
      });

      it('should accept null and undefined values', () => {
        const coin: ICoin = { id: 123 };
        expectedResult = service.addCoinToCollectionIfMissing([], null, coin, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(coin);
      });

      it('should return initial array if no Coin is added', () => {
        const coinCollection: ICoin[] = [{ id: 123 }];
        expectedResult = service.addCoinToCollectionIfMissing(coinCollection, undefined, null);
        expect(expectedResult).toEqual(coinCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
