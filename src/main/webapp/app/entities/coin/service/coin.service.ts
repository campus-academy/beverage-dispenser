import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICoin, getCoinIdentifier } from '../coin.model';

export type EntityResponseType = HttpResponse<ICoin>;
export type EntityArrayResponseType = HttpResponse<ICoin[]>;

@Injectable({ providedIn: 'root' })
export class CoinService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/coins');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(coin: ICoin): Observable<EntityResponseType> {
    return this.http.post<ICoin>(this.resourceUrl, coin, { observe: 'response' });
  }

  update(coin: ICoin): Observable<EntityResponseType> {
    return this.http.put<ICoin>(`${this.resourceUrl}/${getCoinIdentifier(coin) as number}`, coin, { observe: 'response' });
  }

  partialUpdate(coin: ICoin): Observable<EntityResponseType> {
    return this.http.patch<ICoin>(`${this.resourceUrl}/${getCoinIdentifier(coin) as number}`, coin, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICoin>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICoin[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCoinToCollectionIfMissing(coinCollection: ICoin[], ...coinsToCheck: (ICoin | null | undefined)[]): ICoin[] {
    const coins: ICoin[] = coinsToCheck.filter(isPresent);
    if (coins.length > 0) {
      const coinCollectionIdentifiers = coinCollection.map(coinItem => getCoinIdentifier(coinItem)!);
      const coinsToAdd = coins.filter(coinItem => {
        const coinIdentifier = getCoinIdentifier(coinItem);
        if (coinIdentifier == null || coinCollectionIdentifiers.includes(coinIdentifier)) {
          return false;
        }
        coinCollectionIdentifiers.push(coinIdentifier);
        return true;
      });
      return [...coinsToAdd, ...coinCollection];
    }
    return coinCollection;
  }
}
