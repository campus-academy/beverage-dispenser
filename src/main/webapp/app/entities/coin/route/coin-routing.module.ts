import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CoinComponent } from '../list/coin.component';
import { CoinDetailComponent } from '../detail/coin-detail.component';
import { CoinUpdateComponent } from '../update/coin-update.component';
import { CoinRoutingResolveService } from './coin-routing-resolve.service';

const coinRoute: Routes = [
  {
    path: '',
    component: CoinComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CoinDetailComponent,
    resolve: {
      coin: CoinRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CoinUpdateComponent,
    resolve: {
      coin: CoinRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CoinUpdateComponent,
    resolve: {
      coin: CoinRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(coinRoute)],
  exports: [RouterModule],
})
export class CoinRoutingModule {}
