import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICoin, Coin } from '../coin.model';
import { CoinService } from '../service/coin.service';

@Injectable({ providedIn: 'root' })
export class CoinRoutingResolveService implements Resolve<ICoin> {
  constructor(protected service: CoinService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICoin> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((coin: HttpResponse<Coin>) => {
          if (coin.body) {
            return of(coin.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Coin());
  }
}
