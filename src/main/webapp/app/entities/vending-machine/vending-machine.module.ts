import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { VendingMachineComponent } from './list/vending-machine.component';
import { VendingMachineDetailComponent } from './detail/vending-machine-detail.component';
import { VendingMachineUpdateComponent } from './update/vending-machine-update.component';
import { VendingMachineDeleteDialogComponent } from './delete/vending-machine-delete-dialog.component';
import { VendingMachineRoutingModule } from './route/vending-machine-routing.module';

@NgModule({
  imports: [SharedModule, VendingMachineRoutingModule],
  declarations: [
    VendingMachineComponent,
    VendingMachineDetailComponent,
    VendingMachineUpdateComponent,
    VendingMachineDeleteDialogComponent,
  ],
  entryComponents: [VendingMachineDeleteDialogComponent],
})
export class VendingMachineModule {}
