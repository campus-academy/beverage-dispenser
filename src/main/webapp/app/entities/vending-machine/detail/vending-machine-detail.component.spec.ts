import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { VendingMachineDetailComponent } from './vending-machine-detail.component';

describe('VendingMachine Management Detail Component', () => {
  let comp: VendingMachineDetailComponent;
  let fixture: ComponentFixture<VendingMachineDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VendingMachineDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ vendingMachine: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(VendingMachineDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(VendingMachineDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load vendingMachine on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.vendingMachine).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
