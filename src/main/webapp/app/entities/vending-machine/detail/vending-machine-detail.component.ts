import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IVendingMachine } from '../vending-machine.model';

@Component({
  selector: 'jhi-vending-machine-detail',
  templateUrl: './vending-machine-detail.component.html',
})
export class VendingMachineDetailComponent implements OnInit {
  vendingMachine: IVendingMachine | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ vendingMachine }) => {
      this.vendingMachine = vendingMachine;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
