import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IVendingMachine, VendingMachine } from '../vending-machine.model';
import { VendingMachineService } from '../service/vending-machine.service';

@Injectable({ providedIn: 'root' })
export class VendingMachineRoutingResolveService implements Resolve<IVendingMachine> {
  constructor(protected service: VendingMachineService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IVendingMachine> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((vendingMachine: HttpResponse<VendingMachine>) => {
          if (vendingMachine.body) {
            return of(vendingMachine.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new VendingMachine());
  }
}
