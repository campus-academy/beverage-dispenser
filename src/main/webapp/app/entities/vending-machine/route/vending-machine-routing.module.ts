import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { VendingMachineComponent } from '../list/vending-machine.component';
import { VendingMachineDetailComponent } from '../detail/vending-machine-detail.component';
import { VendingMachineUpdateComponent } from '../update/vending-machine-update.component';
import { VendingMachineRoutingResolveService } from './vending-machine-routing-resolve.service';

const vendingMachineRoute: Routes = [
  {
    path: '',
    component: VendingMachineComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: VendingMachineDetailComponent,
    resolve: {
      vendingMachine: VendingMachineRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: VendingMachineUpdateComponent,
    resolve: {
      vendingMachine: VendingMachineRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: VendingMachineUpdateComponent,
    resolve: {
      vendingMachine: VendingMachineRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(vendingMachineRoute)],
  exports: [RouterModule],
})
export class VendingMachineRoutingModule {}
