import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { VendingMachineService } from '../service/vending-machine.service';
import { IVendingMachine, VendingMachine } from '../vending-machine.model';
import { ICoinAcceptor } from 'app/entities/coin-acceptor/coin-acceptor.model';
import { CoinAcceptorService } from 'app/entities/coin-acceptor/service/coin-acceptor.service';
import { IDrink } from 'app/entities/drink/drink.model';
import { DrinkService } from 'app/entities/drink/service/drink.service';
import { IInfrastructure } from 'app/entities/infrastructure/infrastructure.model';
import { InfrastructureService } from 'app/entities/infrastructure/service/infrastructure.service';

import { VendingMachineUpdateComponent } from './vending-machine-update.component';

describe('VendingMachine Management Update Component', () => {
  let comp: VendingMachineUpdateComponent;
  let fixture: ComponentFixture<VendingMachineUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let vendingMachineService: VendingMachineService;
  let coinAcceptorService: CoinAcceptorService;
  let drinkService: DrinkService;
  let infrastructureService: InfrastructureService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [VendingMachineUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(VendingMachineUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(VendingMachineUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    vendingMachineService = TestBed.inject(VendingMachineService);
    coinAcceptorService = TestBed.inject(CoinAcceptorService);
    drinkService = TestBed.inject(DrinkService);
    infrastructureService = TestBed.inject(InfrastructureService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call coinAcceptor query and add missing value', () => {
      const vendingMachine: IVendingMachine = { id: 456 };
      const coinAcceptor: ICoinAcceptor = { id: 63021 };
      vendingMachine.coinAcceptor = coinAcceptor;

      const coinAcceptorCollection: ICoinAcceptor[] = [{ id: 11498 }];
      jest.spyOn(coinAcceptorService, 'query').mockReturnValue(of(new HttpResponse({ body: coinAcceptorCollection })));
      const expectedCollection: ICoinAcceptor[] = [coinAcceptor, ...coinAcceptorCollection];
      jest.spyOn(coinAcceptorService, 'addCoinAcceptorToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ vendingMachine });
      comp.ngOnInit();

      expect(coinAcceptorService.query).toHaveBeenCalled();
      expect(coinAcceptorService.addCoinAcceptorToCollectionIfMissing).toHaveBeenCalledWith(coinAcceptorCollection, coinAcceptor);
      expect(comp.coinAcceptorsCollection).toEqual(expectedCollection);
    });

    it('Should call Drink query and add missing value', () => {
      const vendingMachine: IVendingMachine = { id: 456 };
      const drinks: IDrink[] = [{ id: 49958 }];
      vendingMachine.drinks = drinks;

      const drinkCollection: IDrink[] = [{ id: 57806 }];
      jest.spyOn(drinkService, 'query').mockReturnValue(of(new HttpResponse({ body: drinkCollection })));
      const additionalDrinks = [...drinks];
      const expectedCollection: IDrink[] = [...additionalDrinks, ...drinkCollection];
      jest.spyOn(drinkService, 'addDrinkToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ vendingMachine });
      comp.ngOnInit();

      expect(drinkService.query).toHaveBeenCalled();
      expect(drinkService.addDrinkToCollectionIfMissing).toHaveBeenCalledWith(drinkCollection, ...additionalDrinks);
      expect(comp.drinksSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Infrastructure query and add missing value', () => {
      const vendingMachine: IVendingMachine = { id: 456 };
      const infrastructure: IInfrastructure = { id: 7496 };
      vendingMachine.infrastructure = infrastructure;

      const infrastructureCollection: IInfrastructure[] = [{ id: 38227 }];
      jest.spyOn(infrastructureService, 'query').mockReturnValue(of(new HttpResponse({ body: infrastructureCollection })));
      const additionalInfrastructures = [infrastructure];
      const expectedCollection: IInfrastructure[] = [...additionalInfrastructures, ...infrastructureCollection];
      jest.spyOn(infrastructureService, 'addInfrastructureToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ vendingMachine });
      comp.ngOnInit();

      expect(infrastructureService.query).toHaveBeenCalled();
      expect(infrastructureService.addInfrastructureToCollectionIfMissing).toHaveBeenCalledWith(
        infrastructureCollection,
        ...additionalInfrastructures
      );
      expect(comp.infrastructuresSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const vendingMachine: IVendingMachine = { id: 456 };
      const coinAcceptor: ICoinAcceptor = { id: 38675 };
      vendingMachine.coinAcceptor = coinAcceptor;
      const drinks: IDrink = { id: 81456 };
      vendingMachine.drinks = [drinks];
      const infrastructure: IInfrastructure = { id: 26908 };
      vendingMachine.infrastructure = infrastructure;

      activatedRoute.data = of({ vendingMachine });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(vendingMachine));
      expect(comp.coinAcceptorsCollection).toContain(coinAcceptor);
      expect(comp.drinksSharedCollection).toContain(drinks);
      expect(comp.infrastructuresSharedCollection).toContain(infrastructure);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<VendingMachine>>();
      const vendingMachine = { id: 123 };
      jest.spyOn(vendingMachineService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ vendingMachine });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: vendingMachine }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(vendingMachineService.update).toHaveBeenCalledWith(vendingMachine);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<VendingMachine>>();
      const vendingMachine = new VendingMachine();
      jest.spyOn(vendingMachineService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ vendingMachine });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: vendingMachine }));
      saveSubject.complete();

      // THEN
      expect(vendingMachineService.create).toHaveBeenCalledWith(vendingMachine);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<VendingMachine>>();
      const vendingMachine = { id: 123 };
      jest.spyOn(vendingMachineService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ vendingMachine });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(vendingMachineService.update).toHaveBeenCalledWith(vendingMachine);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackCoinAcceptorById', () => {
      it('Should return tracked CoinAcceptor primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCoinAcceptorById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackDrinkById', () => {
      it('Should return tracked Drink primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackDrinkById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackInfrastructureById', () => {
      it('Should return tracked Infrastructure primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackInfrastructureById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });

  describe('Getting selected relationships', () => {
    describe('getSelectedDrink', () => {
      it('Should return option if no Drink is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedDrink(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected Drink for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedDrink(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this Drink is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedDrink(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });
  });
});
