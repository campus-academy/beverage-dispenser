import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IVendingMachine, VendingMachine } from '../vending-machine.model';
import { VendingMachineService } from '../service/vending-machine.service';
import { ICoinAcceptor } from 'app/entities/coin-acceptor/coin-acceptor.model';
import { CoinAcceptorService } from 'app/entities/coin-acceptor/service/coin-acceptor.service';
import { IDrink } from 'app/entities/drink/drink.model';
import { DrinkService } from 'app/entities/drink/service/drink.service';
import { IInfrastructure } from 'app/entities/infrastructure/infrastructure.model';
import { InfrastructureService } from 'app/entities/infrastructure/service/infrastructure.service';

@Component({
  selector: 'jhi-vending-machine-update',
  templateUrl: './vending-machine-update.component.html',
})
export class VendingMachineUpdateComponent implements OnInit {
  isSaving = false;

  coinAcceptorsCollection: ICoinAcceptor[] = [];
  drinksSharedCollection: IDrink[] = [];
  infrastructuresSharedCollection: IInfrastructure[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    serialNumber: [],
    brand: [],
    model: [],
    coinAcceptor: [],
    drinks: [],
    infrastructure: [],
  });

  constructor(
    protected vendingMachineService: VendingMachineService,
    protected coinAcceptorService: CoinAcceptorService,
    protected drinkService: DrinkService,
    protected infrastructureService: InfrastructureService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ vendingMachine }) => {
      this.updateForm(vendingMachine);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const vendingMachine = this.createFromForm();
    if (vendingMachine.id !== undefined) {
      this.subscribeToSaveResponse(this.vendingMachineService.update(vendingMachine));
    } else {
      this.subscribeToSaveResponse(this.vendingMachineService.create(vendingMachine));
    }
  }

  trackCoinAcceptorById(index: number, item: ICoinAcceptor): number {
    return item.id!;
  }

  trackDrinkById(index: number, item: IDrink): number {
    return item.id!;
  }

  trackInfrastructureById(index: number, item: IInfrastructure): number {
    return item.id!;
  }

  getSelectedDrink(option: IDrink, selectedVals?: IDrink[]): IDrink {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVendingMachine>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(vendingMachine: IVendingMachine): void {
    this.editForm.patchValue({
      id: vendingMachine.id,
      name: vendingMachine.name,
      serialNumber: vendingMachine.serialNumber,
      brand: vendingMachine.brand,
      model: vendingMachine.model,
      coinAcceptor: vendingMachine.coinAcceptor,
      drinks: vendingMachine.drinks,
      infrastructure: vendingMachine.infrastructure,
    });

    this.coinAcceptorsCollection = this.coinAcceptorService.addCoinAcceptorToCollectionIfMissing(
      this.coinAcceptorsCollection,
      vendingMachine.coinAcceptor
    );
    this.drinksSharedCollection = this.drinkService.addDrinkToCollectionIfMissing(
      this.drinksSharedCollection,
      ...(vendingMachine.drinks ?? [])
    );
    this.infrastructuresSharedCollection = this.infrastructureService.addInfrastructureToCollectionIfMissing(
      this.infrastructuresSharedCollection,
      vendingMachine.infrastructure
    );
  }

  protected loadRelationshipsOptions(): void {
    this.coinAcceptorService
      .query({ filter: 'vendingmachine-is-null' })
      .pipe(map((res: HttpResponse<ICoinAcceptor[]>) => res.body ?? []))
      .pipe(
        map((coinAcceptors: ICoinAcceptor[]) =>
          this.coinAcceptorService.addCoinAcceptorToCollectionIfMissing(coinAcceptors, this.editForm.get('coinAcceptor')!.value)
        )
      )
      .subscribe((coinAcceptors: ICoinAcceptor[]) => (this.coinAcceptorsCollection = coinAcceptors));

    this.drinkService
      .query()
      .pipe(map((res: HttpResponse<IDrink[]>) => res.body ?? []))
      .pipe(
        map((drinks: IDrink[]) => this.drinkService.addDrinkToCollectionIfMissing(drinks, ...(this.editForm.get('drinks')!.value ?? [])))
      )
      .subscribe((drinks: IDrink[]) => (this.drinksSharedCollection = drinks));

    this.infrastructureService
      .query()
      .pipe(map((res: HttpResponse<IInfrastructure[]>) => res.body ?? []))
      .pipe(
        map((infrastructures: IInfrastructure[]) =>
          this.infrastructureService.addInfrastructureToCollectionIfMissing(infrastructures, this.editForm.get('infrastructure')!.value)
        )
      )
      .subscribe((infrastructures: IInfrastructure[]) => (this.infrastructuresSharedCollection = infrastructures));
  }

  protected createFromForm(): IVendingMachine {
    return {
      ...new VendingMachine(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      serialNumber: this.editForm.get(['serialNumber'])!.value,
      brand: this.editForm.get(['brand'])!.value,
      model: this.editForm.get(['model'])!.value,
      coinAcceptor: this.editForm.get(['coinAcceptor'])!.value,
      drinks: this.editForm.get(['drinks'])!.value,
      infrastructure: this.editForm.get(['infrastructure'])!.value,
    };
  }
}
