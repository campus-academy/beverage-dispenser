import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IVendingMachine } from '../vending-machine.model';
import { VendingMachineService } from '../service/vending-machine.service';

@Component({
  templateUrl: './vending-machine-delete-dialog.component.html',
})
export class VendingMachineDeleteDialogComponent {
  vendingMachine?: IVendingMachine;

  constructor(protected vendingMachineService: VendingMachineService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.vendingMachineService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
