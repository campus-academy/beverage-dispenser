import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { VendingMachineService } from '../service/vending-machine.service';

import { VendingMachineComponent } from './vending-machine.component';

describe('VendingMachine Management Component', () => {
  let comp: VendingMachineComponent;
  let fixture: ComponentFixture<VendingMachineComponent>;
  let service: VendingMachineService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [VendingMachineComponent],
    })
      .overrideTemplate(VendingMachineComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(VendingMachineComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(VendingMachineService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.vendingMachines?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
