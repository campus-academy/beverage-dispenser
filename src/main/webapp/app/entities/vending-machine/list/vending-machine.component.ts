import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IVendingMachine } from '../vending-machine.model';
import { VendingMachineService } from '../service/vending-machine.service';
import { VendingMachineDeleteDialogComponent } from '../delete/vending-machine-delete-dialog.component';

@Component({
  selector: 'jhi-vending-machine',
  templateUrl: './vending-machine.component.html',
})
export class VendingMachineComponent implements OnInit {
  vendingMachines?: IVendingMachine[];
  isLoading = false;

  constructor(protected vendingMachineService: VendingMachineService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.vendingMachineService.query().subscribe({
      next: (res: HttpResponse<IVendingMachine[]>) => {
        this.isLoading = false;
        this.vendingMachines = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IVendingMachine): number {
    return item.id!;
  }

  delete(vendingMachine: IVendingMachine): void {
    const modalRef = this.modalService.open(VendingMachineDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.vendingMachine = vendingMachine;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
