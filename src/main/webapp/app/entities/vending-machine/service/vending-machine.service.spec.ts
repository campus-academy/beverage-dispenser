import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IVendingMachine, VendingMachine } from '../vending-machine.model';

import { VendingMachineService } from './vending-machine.service';

describe('VendingMachine Service', () => {
  let service: VendingMachineService;
  let httpMock: HttpTestingController;
  let elemDefault: IVendingMachine;
  let expectedResult: IVendingMachine | IVendingMachine[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(VendingMachineService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      serialNumber: 'AAAAAAA',
      brand: 'AAAAAAA',
      model: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a VendingMachine', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new VendingMachine()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a VendingMachine', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          serialNumber: 'BBBBBB',
          brand: 'BBBBBB',
          model: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a VendingMachine', () => {
      const patchObject = Object.assign(
        {
          name: 'BBBBBB',
        },
        new VendingMachine()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of VendingMachine', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          serialNumber: 'BBBBBB',
          brand: 'BBBBBB',
          model: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a VendingMachine', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addVendingMachineToCollectionIfMissing', () => {
      it('should add a VendingMachine to an empty array', () => {
        const vendingMachine: IVendingMachine = { id: 123 };
        expectedResult = service.addVendingMachineToCollectionIfMissing([], vendingMachine);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(vendingMachine);
      });

      it('should not add a VendingMachine to an array that contains it', () => {
        const vendingMachine: IVendingMachine = { id: 123 };
        const vendingMachineCollection: IVendingMachine[] = [
          {
            ...vendingMachine,
          },
          { id: 456 },
        ];
        expectedResult = service.addVendingMachineToCollectionIfMissing(vendingMachineCollection, vendingMachine);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a VendingMachine to an array that doesn't contain it", () => {
        const vendingMachine: IVendingMachine = { id: 123 };
        const vendingMachineCollection: IVendingMachine[] = [{ id: 456 }];
        expectedResult = service.addVendingMachineToCollectionIfMissing(vendingMachineCollection, vendingMachine);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(vendingMachine);
      });

      it('should add only unique VendingMachine to an array', () => {
        const vendingMachineArray: IVendingMachine[] = [{ id: 123 }, { id: 456 }, { id: 80007 }];
        const vendingMachineCollection: IVendingMachine[] = [{ id: 123 }];
        expectedResult = service.addVendingMachineToCollectionIfMissing(vendingMachineCollection, ...vendingMachineArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const vendingMachine: IVendingMachine = { id: 123 };
        const vendingMachine2: IVendingMachine = { id: 456 };
        expectedResult = service.addVendingMachineToCollectionIfMissing([], vendingMachine, vendingMachine2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(vendingMachine);
        expect(expectedResult).toContain(vendingMachine2);
      });

      it('should accept null and undefined values', () => {
        const vendingMachine: IVendingMachine = { id: 123 };
        expectedResult = service.addVendingMachineToCollectionIfMissing([], null, vendingMachine, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(vendingMachine);
      });

      it('should return initial array if no VendingMachine is added', () => {
        const vendingMachineCollection: IVendingMachine[] = [{ id: 123 }];
        expectedResult = service.addVendingMachineToCollectionIfMissing(vendingMachineCollection, undefined, null);
        expect(expectedResult).toEqual(vendingMachineCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
