import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IVendingMachine, getVendingMachineIdentifier } from '../vending-machine.model';

export type EntityResponseType = HttpResponse<IVendingMachine>;
export type EntityArrayResponseType = HttpResponse<IVendingMachine[]>;

@Injectable({ providedIn: 'root' })
export class VendingMachineService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/vending-machines');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(vendingMachine: IVendingMachine): Observable<EntityResponseType> {
    return this.http.post<IVendingMachine>(this.resourceUrl, vendingMachine, { observe: 'response' });
  }

  update(vendingMachine: IVendingMachine): Observable<EntityResponseType> {
    return this.http.put<IVendingMachine>(`${this.resourceUrl}/${getVendingMachineIdentifier(vendingMachine) as number}`, vendingMachine, {
      observe: 'response',
    });
  }

  partialUpdate(vendingMachine: IVendingMachine): Observable<EntityResponseType> {
    return this.http.patch<IVendingMachine>(
      `${this.resourceUrl}/${getVendingMachineIdentifier(vendingMachine) as number}`,
      vendingMachine,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IVendingMachine>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IVendingMachine[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addVendingMachineToCollectionIfMissing(
    vendingMachineCollection: IVendingMachine[],
    ...vendingMachinesToCheck: (IVendingMachine | null | undefined)[]
  ): IVendingMachine[] {
    const vendingMachines: IVendingMachine[] = vendingMachinesToCheck.filter(isPresent);
    if (vendingMachines.length > 0) {
      const vendingMachineCollectionIdentifiers = vendingMachineCollection.map(
        vendingMachineItem => getVendingMachineIdentifier(vendingMachineItem)!
      );
      const vendingMachinesToAdd = vendingMachines.filter(vendingMachineItem => {
        const vendingMachineIdentifier = getVendingMachineIdentifier(vendingMachineItem);
        if (vendingMachineIdentifier == null || vendingMachineCollectionIdentifiers.includes(vendingMachineIdentifier)) {
          return false;
        }
        vendingMachineCollectionIdentifiers.push(vendingMachineIdentifier);
        return true;
      });
      return [...vendingMachinesToAdd, ...vendingMachineCollection];
    }
    return vendingMachineCollection;
  }
}
