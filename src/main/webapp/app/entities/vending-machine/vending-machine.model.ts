import { ICoinAcceptor } from 'app/entities/coin-acceptor/coin-acceptor.model';
import { IDrink } from 'app/entities/drink/drink.model';
import { IInfrastructure } from 'app/entities/infrastructure/infrastructure.model';

export interface IVendingMachine {
  id?: number;
  name?: string | null;
  serialNumber?: string | null;
  brand?: string | null;
  model?: string | null;
  coinAcceptor?: ICoinAcceptor | null;
  drinks?: IDrink[] | null;
  infrastructure?: IInfrastructure | null;
}

export class VendingMachine implements IVendingMachine {
  constructor(
    public id?: number,
    public name?: string | null,
    public serialNumber?: string | null,
    public brand?: string | null,
    public model?: string | null,
    public coinAcceptor?: ICoinAcceptor | null,
    public drinks?: IDrink[] | null,
    public infrastructure?: IInfrastructure | null
  ) {}
}

export function getVendingMachineIdentifier(vendingMachine: IVendingMachine): number | undefined {
  return vendingMachine.id;
}
