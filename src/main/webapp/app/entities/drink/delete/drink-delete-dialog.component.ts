import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IDrink } from '../drink.model';
import { DrinkService } from '../service/drink.service';

@Component({
  templateUrl: './drink-delete-dialog.component.html',
})
export class DrinkDeleteDialogComponent {
  drink?: IDrink;

  constructor(protected drinkService: DrinkService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.drinkService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
