import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDrink } from '../drink.model';
import { DrinkService } from '../service/drink.service';
import { DrinkDeleteDialogComponent } from '../delete/drink-delete-dialog.component';

@Component({
  selector: 'jhi-drink',
  templateUrl: './drink.component.html',
})
export class DrinkComponent implements OnInit {
  drinks?: IDrink[];
  isLoading = false;

  constructor(protected drinkService: DrinkService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.drinkService.query().subscribe({
      next: (res: HttpResponse<IDrink[]>) => {
        this.isLoading = false;
        this.drinks = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IDrink): number {
    return item.id!;
  }

  delete(drink: IDrink): void {
    const modalRef = this.modalService.open(DrinkDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.drink = drink;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
