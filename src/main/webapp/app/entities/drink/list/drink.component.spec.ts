import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { DrinkService } from '../service/drink.service';

import { DrinkComponent } from './drink.component';

describe('Drink Management Component', () => {
  let comp: DrinkComponent;
  let fixture: ComponentFixture<DrinkComponent>;
  let service: DrinkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [DrinkComponent],
    })
      .overrideTemplate(DrinkComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(DrinkComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(DrinkService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.drinks?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
