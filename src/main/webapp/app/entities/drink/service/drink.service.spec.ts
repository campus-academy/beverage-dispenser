import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IDrink, Drink } from '../drink.model';

import { DrinkService } from './drink.service';

describe('Drink Service', () => {
  let service: DrinkService;
  let httpMock: HttpTestingController;
  let elemDefault: IDrink;
  let expectedResult: IDrink | IDrink[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(DrinkService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      price: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Drink', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Drink()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Drink', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          price: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Drink', () => {
      const patchObject = Object.assign(
        {
          name: 'BBBBBB',
        },
        new Drink()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Drink', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          price: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Drink', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addDrinkToCollectionIfMissing', () => {
      it('should add a Drink to an empty array', () => {
        const drink: IDrink = { id: 123 };
        expectedResult = service.addDrinkToCollectionIfMissing([], drink);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(drink);
      });

      it('should not add a Drink to an array that contains it', () => {
        const drink: IDrink = { id: 123 };
        const drinkCollection: IDrink[] = [
          {
            ...drink,
          },
          { id: 456 },
        ];
        expectedResult = service.addDrinkToCollectionIfMissing(drinkCollection, drink);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Drink to an array that doesn't contain it", () => {
        const drink: IDrink = { id: 123 };
        const drinkCollection: IDrink[] = [{ id: 456 }];
        expectedResult = service.addDrinkToCollectionIfMissing(drinkCollection, drink);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(drink);
      });

      it('should add only unique Drink to an array', () => {
        const drinkArray: IDrink[] = [{ id: 123 }, { id: 456 }, { id: 20593 }];
        const drinkCollection: IDrink[] = [{ id: 123 }];
        expectedResult = service.addDrinkToCollectionIfMissing(drinkCollection, ...drinkArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const drink: IDrink = { id: 123 };
        const drink2: IDrink = { id: 456 };
        expectedResult = service.addDrinkToCollectionIfMissing([], drink, drink2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(drink);
        expect(expectedResult).toContain(drink2);
      });

      it('should accept null and undefined values', () => {
        const drink: IDrink = { id: 123 };
        expectedResult = service.addDrinkToCollectionIfMissing([], null, drink, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(drink);
      });

      it('should return initial array if no Drink is added', () => {
        const drinkCollection: IDrink[] = [{ id: 123 }];
        expectedResult = service.addDrinkToCollectionIfMissing(drinkCollection, undefined, null);
        expect(expectedResult).toEqual(drinkCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
