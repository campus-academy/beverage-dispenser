import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDrink, getDrinkIdentifier } from '../drink.model';

export type EntityResponseType = HttpResponse<IDrink>;
export type EntityArrayResponseType = HttpResponse<IDrink[]>;

@Injectable({ providedIn: 'root' })
export class DrinkService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/drinks');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(drink: IDrink): Observable<EntityResponseType> {
    return this.http.post<IDrink>(this.resourceUrl, drink, { observe: 'response' });
  }

  update(drink: IDrink): Observable<EntityResponseType> {
    return this.http.put<IDrink>(`${this.resourceUrl}/${getDrinkIdentifier(drink) as number}`, drink, { observe: 'response' });
  }

  partialUpdate(drink: IDrink): Observable<EntityResponseType> {
    return this.http.patch<IDrink>(`${this.resourceUrl}/${getDrinkIdentifier(drink) as number}`, drink, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDrink>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDrink[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addDrinkToCollectionIfMissing(drinkCollection: IDrink[], ...drinksToCheck: (IDrink | null | undefined)[]): IDrink[] {
    const drinks: IDrink[] = drinksToCheck.filter(isPresent);
    if (drinks.length > 0) {
      const drinkCollectionIdentifiers = drinkCollection.map(drinkItem => getDrinkIdentifier(drinkItem)!);
      const drinksToAdd = drinks.filter(drinkItem => {
        const drinkIdentifier = getDrinkIdentifier(drinkItem);
        if (drinkIdentifier == null || drinkCollectionIdentifiers.includes(drinkIdentifier)) {
          return false;
        }
        drinkCollectionIdentifiers.push(drinkIdentifier);
        return true;
      });
      return [...drinksToAdd, ...drinkCollection];
    }
    return drinkCollection;
  }

  makeDrink(selectedDrink: any): any {
    return this.http.put(`${this.resourceUrl}/make/${selectedDrink}`, null);
  }
}
