import { IIngredient } from 'app/entities/ingredient/ingredient.model';
import { IVendingMachine } from 'app/entities/vending-machine/vending-machine.model';

export interface IDrink {
  id?: number;
  name?: string | null;
  price?: number | null;
  ingredients?: IIngredient[] | null;
  vendingMachines?: IVendingMachine[] | null;
}

export class Drink implements IDrink {
  constructor(
    public id?: number,
    public name?: string | null,
    public price?: number | null,
    public ingredients?: IIngredient[] | null,
    public vendingMachines?: IVendingMachine[] | null
  ) {}
}

export function getDrinkIdentifier(drink: IDrink): number | undefined {
  return drink.id;
}
