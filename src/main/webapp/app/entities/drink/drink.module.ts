import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { DrinkComponent } from './list/drink.component';
import { DrinkDetailComponent } from './detail/drink-detail.component';
import { DrinkUpdateComponent } from './update/drink-update.component';
import { DrinkDeleteDialogComponent } from './delete/drink-delete-dialog.component';
import { DrinkRoutingModule } from './route/drink-routing.module';

@NgModule({
  imports: [SharedModule, DrinkRoutingModule],
  declarations: [DrinkComponent, DrinkDetailComponent, DrinkUpdateComponent, DrinkDeleteDialogComponent],
  entryComponents: [DrinkDeleteDialogComponent],
})
export class DrinkModule {}
