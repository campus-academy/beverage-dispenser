import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDrink } from '../drink.model';

@Component({
  selector: 'jhi-drink-detail',
  templateUrl: './drink-detail.component.html',
})
export class DrinkDetailComponent implements OnInit {
  drink: IDrink | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ drink }) => {
      this.drink = drink;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
