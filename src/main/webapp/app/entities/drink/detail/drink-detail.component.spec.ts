import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DrinkDetailComponent } from './drink-detail.component';

describe('Drink Management Detail Component', () => {
  let comp: DrinkDetailComponent;
  let fixture: ComponentFixture<DrinkDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DrinkDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ drink: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(DrinkDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(DrinkDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load drink on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.drink).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
