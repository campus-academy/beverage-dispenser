import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DrinkComponent } from '../list/drink.component';
import { DrinkDetailComponent } from '../detail/drink-detail.component';
import { DrinkUpdateComponent } from '../update/drink-update.component';
import { DrinkRoutingResolveService } from './drink-routing-resolve.service';

const drinkRoute: Routes = [
  {
    path: '',
    component: DrinkComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DrinkDetailComponent,
    resolve: {
      drink: DrinkRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DrinkUpdateComponent,
    resolve: {
      drink: DrinkRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DrinkUpdateComponent,
    resolve: {
      drink: DrinkRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(drinkRoute)],
  exports: [RouterModule],
})
export class DrinkRoutingModule {}
