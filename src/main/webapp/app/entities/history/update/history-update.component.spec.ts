import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { HistoryService } from '../service/history.service';
import { IHistory, History } from '../history.model';
import { IInfrastructure } from 'app/entities/infrastructure/infrastructure.model';
import { InfrastructureService } from 'app/entities/infrastructure/service/infrastructure.service';

import { HistoryUpdateComponent } from './history-update.component';

describe('History Management Update Component', () => {
  let comp: HistoryUpdateComponent;
  let fixture: ComponentFixture<HistoryUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let historyService: HistoryService;
  let infrastructureService: InfrastructureService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [HistoryUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(HistoryUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(HistoryUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    historyService = TestBed.inject(HistoryService);
    infrastructureService = TestBed.inject(InfrastructureService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call infrastructureId query and add missing value', () => {
      const history: IHistory = { id: 456 };
      const infrastructureId: IInfrastructure = { id: 69554 };
      history.infrastructureId = infrastructureId;

      const infrastructureIdCollection: IInfrastructure[] = [{ id: 49732 }];
      jest.spyOn(infrastructureService, 'query').mockReturnValue(of(new HttpResponse({ body: infrastructureIdCollection })));
      const expectedCollection: IInfrastructure[] = [infrastructureId, ...infrastructureIdCollection];
      jest.spyOn(infrastructureService, 'addInfrastructureToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ history });
      comp.ngOnInit();

      expect(infrastructureService.query).toHaveBeenCalled();
      expect(infrastructureService.addInfrastructureToCollectionIfMissing).toHaveBeenCalledWith(
        infrastructureIdCollection,
        infrastructureId
      );
      expect(comp.infrastructureIdsCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const history: IHistory = { id: 456 };
      const infrastructureId: IInfrastructure = { id: 20716 };
      history.infrastructureId = infrastructureId;

      activatedRoute.data = of({ history });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(history));
      expect(comp.infrastructureIdsCollection).toContain(infrastructureId);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<History>>();
      const history = { id: 123 };
      jest.spyOn(historyService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ history });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: history }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(historyService.update).toHaveBeenCalledWith(history);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<History>>();
      const history = new History();
      jest.spyOn(historyService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ history });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: history }));
      saveSubject.complete();

      // THEN
      expect(historyService.create).toHaveBeenCalledWith(history);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<History>>();
      const history = { id: 123 };
      jest.spyOn(historyService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ history });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(historyService.update).toHaveBeenCalledWith(history);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackInfrastructureById', () => {
      it('Should return tracked Infrastructure primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackInfrastructureById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
