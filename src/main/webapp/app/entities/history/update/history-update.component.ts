import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IHistory, History } from '../history.model';
import { HistoryService } from '../service/history.service';
import { IInfrastructure } from 'app/entities/infrastructure/infrastructure.model';
import { InfrastructureService } from 'app/entities/infrastructure/service/infrastructure.service';
import { Actions } from 'app/entities/enumerations/actions.model';

@Component({
  selector: 'jhi-history-update',
  templateUrl: './history-update.component.html',
})
export class HistoryUpdateComponent implements OnInit {
  isSaving = false;
  actionsValues = Object.keys(Actions);

  infrastructureIdsCollection: IInfrastructure[] = [];

  editForm = this.fb.group({
    id: [],
    machineId: [],
    total: [],
    drinkId: [],
    ingredientId: [],
    description: [],
    action: [],
    infrastructureId: [],
  });

  constructor(
    protected historyService: HistoryService,
    protected infrastructureService: InfrastructureService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ history }) => {
      this.updateForm(history);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const history = this.createFromForm();
    if (history.id !== undefined) {
      this.subscribeToSaveResponse(this.historyService.update(history));
    } else {
      this.subscribeToSaveResponse(this.historyService.create(history));
    }
  }

  trackInfrastructureById(index: number, item: IInfrastructure): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHistory>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(history: IHistory): void {
    this.editForm.patchValue({
      id: history.id,
      machineId: history.machineId,
      total: history.total,
      drinkId: history.drinkId,
      ingredientId: history.ingredientId,
      description: history.description,
      action: history.action,
      infrastructureId: history.infrastructureId,
    });

    this.infrastructureIdsCollection = this.infrastructureService.addInfrastructureToCollectionIfMissing(
      this.infrastructureIdsCollection,
      history.infrastructureId
    );
  }

  protected loadRelationshipsOptions(): void {
    this.infrastructureService
      .query({ filter: 'history-is-null' })
      .pipe(map((res: HttpResponse<IInfrastructure[]>) => res.body ?? []))
      .pipe(
        map((infrastructures: IInfrastructure[]) =>
          this.infrastructureService.addInfrastructureToCollectionIfMissing(infrastructures, this.editForm.get('infrastructureId')!.value)
        )
      )
      .subscribe((infrastructures: IInfrastructure[]) => (this.infrastructureIdsCollection = infrastructures));
  }

  protected createFromForm(): IHistory {
    return {
      ...new History(),
      id: this.editForm.get(['id'])!.value,
      machineId: this.editForm.get(['machineId'])!.value,
      total: this.editForm.get(['total'])!.value,
      drinkId: this.editForm.get(['drinkId'])!.value,
      ingredientId: this.editForm.get(['ingredientId'])!.value,
      description: this.editForm.get(['description'])!.value,
      action: this.editForm.get(['action'])!.value,
      infrastructureId: this.editForm.get(['infrastructureId'])!.value,
    };
  }
}
