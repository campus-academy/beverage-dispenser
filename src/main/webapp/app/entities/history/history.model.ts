import { IInfrastructure } from 'app/entities/infrastructure/infrastructure.model';
import { Actions } from 'app/entities/enumerations/actions.model';

export interface IHistory {
  id?: number;
  machineId?: number | null;
  total?: number | null;
  drinkId?: number | null;
  ingredientId?: number | null;
  description?: string | null;
  action?: Actions | null;
  infrastructureId?: IInfrastructure | null;
}

export class History implements IHistory {
  constructor(
    public id?: number,
    public machineId?: number | null,
    public total?: number | null,
    public drinkId?: number | null,
    public ingredientId?: number | null,
    public description?: string | null,
    public action?: Actions | null,
    public infrastructureId?: IInfrastructure | null
  ) {}
}

export function getHistoryIdentifier(history: IHistory): number | undefined {
  return history.id;
}
