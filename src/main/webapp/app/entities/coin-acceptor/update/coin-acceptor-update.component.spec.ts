import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CoinAcceptorService } from '../service/coin-acceptor.service';
import { ICoinAcceptor, CoinAcceptor } from '../coin-acceptor.model';
import { ICoin } from 'app/entities/coin/coin.model';
import { CoinService } from 'app/entities/coin/service/coin.service';

import { CoinAcceptorUpdateComponent } from './coin-acceptor-update.component';

describe('CoinAcceptor Management Update Component', () => {
  let comp: CoinAcceptorUpdateComponent;
  let fixture: ComponentFixture<CoinAcceptorUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let coinAcceptorService: CoinAcceptorService;
  let coinService: CoinService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CoinAcceptorUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CoinAcceptorUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CoinAcceptorUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    coinAcceptorService = TestBed.inject(CoinAcceptorService);
    coinService = TestBed.inject(CoinService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Coin query and add missing value', () => {
      const coinAcceptor: ICoinAcceptor = { id: 456 };
      const coins: ICoin[] = [{ id: 58047 }];
      coinAcceptor.coins = coins;

      const coinCollection: ICoin[] = [{ id: 68671 }];
      jest.spyOn(coinService, 'query').mockReturnValue(of(new HttpResponse({ body: coinCollection })));
      const additionalCoins = [...coins];
      const expectedCollection: ICoin[] = [...additionalCoins, ...coinCollection];
      jest.spyOn(coinService, 'addCoinToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ coinAcceptor });
      comp.ngOnInit();

      expect(coinService.query).toHaveBeenCalled();
      expect(coinService.addCoinToCollectionIfMissing).toHaveBeenCalledWith(coinCollection, ...additionalCoins);
      expect(comp.coinsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const coinAcceptor: ICoinAcceptor = { id: 456 };
      const coins: ICoin = { id: 95199 };
      coinAcceptor.coins = [coins];

      activatedRoute.data = of({ coinAcceptor });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(coinAcceptor));
      expect(comp.coinsSharedCollection).toContain(coins);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CoinAcceptor>>();
      const coinAcceptor = { id: 123 };
      jest.spyOn(coinAcceptorService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ coinAcceptor });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: coinAcceptor }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(coinAcceptorService.update).toHaveBeenCalledWith(coinAcceptor);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CoinAcceptor>>();
      const coinAcceptor = new CoinAcceptor();
      jest.spyOn(coinAcceptorService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ coinAcceptor });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: coinAcceptor }));
      saveSubject.complete();

      // THEN
      expect(coinAcceptorService.create).toHaveBeenCalledWith(coinAcceptor);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CoinAcceptor>>();
      const coinAcceptor = { id: 123 };
      jest.spyOn(coinAcceptorService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ coinAcceptor });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(coinAcceptorService.update).toHaveBeenCalledWith(coinAcceptor);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackCoinById', () => {
      it('Should return tracked Coin primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCoinById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });

  describe('Getting selected relationships', () => {
    describe('getSelectedCoin', () => {
      it('Should return option if no Coin is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedCoin(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected Coin for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedCoin(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this Coin is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedCoin(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });
  });
});
