import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ICoinAcceptor, CoinAcceptor } from '../coin-acceptor.model';
import { CoinAcceptorService } from '../service/coin-acceptor.service';
import { ICoin } from 'app/entities/coin/coin.model';
import { CoinService } from 'app/entities/coin/service/coin.service';

@Component({
  selector: 'jhi-coin-acceptor-update',
  templateUrl: './coin-acceptor-update.component.html',
})
export class CoinAcceptorUpdateComponent implements OnInit {
  isSaving = false;

  coinsSharedCollection: ICoin[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    requestedCurrency: [],
    coins: [],
  });

  constructor(
    protected coinAcceptorService: CoinAcceptorService,
    protected coinService: CoinService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ coinAcceptor }) => {
      this.updateForm(coinAcceptor);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const coinAcceptor = this.createFromForm();
    if (coinAcceptor.id !== undefined) {
      this.subscribeToSaveResponse(this.coinAcceptorService.update(coinAcceptor));
    } else {
      this.subscribeToSaveResponse(this.coinAcceptorService.create(coinAcceptor));
    }
  }

  trackCoinById(index: number, item: ICoin): number {
    return item.id!;
  }

  getSelectedCoin(option: ICoin, selectedVals?: ICoin[]): ICoin {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICoinAcceptor>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(coinAcceptor: ICoinAcceptor): void {
    this.editForm.patchValue({
      id: coinAcceptor.id,
      name: coinAcceptor.name,
      requestedCurrency: coinAcceptor.requestedCurrency,
      coins: coinAcceptor.coins,
    });

    this.coinsSharedCollection = this.coinService.addCoinToCollectionIfMissing(this.coinsSharedCollection, ...(coinAcceptor.coins ?? []));
  }

  protected loadRelationshipsOptions(): void {
    this.coinService
      .query()
      .pipe(map((res: HttpResponse<ICoin[]>) => res.body ?? []))
      .pipe(map((coins: ICoin[]) => this.coinService.addCoinToCollectionIfMissing(coins, ...(this.editForm.get('coins')!.value ?? []))))
      .subscribe((coins: ICoin[]) => (this.coinsSharedCollection = coins));
  }

  protected createFromForm(): ICoinAcceptor {
    return {
      ...new CoinAcceptor(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      requestedCurrency: this.editForm.get(['requestedCurrency'])!.value,
      coins: this.editForm.get(['coins'])!.value,
    };
  }
}
