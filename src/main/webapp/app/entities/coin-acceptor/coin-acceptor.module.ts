import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CoinAcceptorComponent } from './list/coin-acceptor.component';
import { CoinAcceptorDetailComponent } from './detail/coin-acceptor-detail.component';
import { CoinAcceptorUpdateComponent } from './update/coin-acceptor-update.component';
import { CoinAcceptorDeleteDialogComponent } from './delete/coin-acceptor-delete-dialog.component';
import { CoinAcceptorRoutingModule } from './route/coin-acceptor-routing.module';

@NgModule({
  imports: [SharedModule, CoinAcceptorRoutingModule],
  declarations: [CoinAcceptorComponent, CoinAcceptorDetailComponent, CoinAcceptorUpdateComponent, CoinAcceptorDeleteDialogComponent],
  entryComponents: [CoinAcceptorDeleteDialogComponent],
})
export class CoinAcceptorModule {}
