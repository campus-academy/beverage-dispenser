import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICoinAcceptor } from '../coin-acceptor.model';

@Component({
  selector: 'jhi-coin-acceptor-detail',
  templateUrl: './coin-acceptor-detail.component.html',
})
export class CoinAcceptorDetailComponent implements OnInit {
  coinAcceptor: ICoinAcceptor | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ coinAcceptor }) => {
      this.coinAcceptor = coinAcceptor;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
