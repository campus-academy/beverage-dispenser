import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CoinAcceptorDetailComponent } from './coin-acceptor-detail.component';

describe('CoinAcceptor Management Detail Component', () => {
  let comp: CoinAcceptorDetailComponent;
  let fixture: ComponentFixture<CoinAcceptorDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CoinAcceptorDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ coinAcceptor: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CoinAcceptorDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CoinAcceptorDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load coinAcceptor on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.coinAcceptor).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
