import { ICoin } from 'app/entities/coin/coin.model';
import { IVendingMachine } from 'app/entities/vending-machine/vending-machine.model';

export interface ICoinAcceptor {
  id?: number;
  name?: string | null;
  requestedCurrency?: number | null;
  coins?: ICoin[] | null;
  vendingMachine?: IVendingMachine | null;
}

export class CoinAcceptor implements ICoinAcceptor {
  constructor(
    public id?: number,
    public name?: string | null,
    public requestedCurrency?: number | null,
    public coins?: ICoin[] | null,
    public vendingMachine?: IVendingMachine | null
  ) {}
}

export function getCoinAcceptorIdentifier(coinAcceptor: ICoinAcceptor): number | undefined {
  return coinAcceptor.id;
}
