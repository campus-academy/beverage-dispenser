import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { CoinAcceptorService } from '../service/coin-acceptor.service';

import { CoinAcceptorComponent } from './coin-acceptor.component';

describe('CoinAcceptor Management Component', () => {
  let comp: CoinAcceptorComponent;
  let fixture: ComponentFixture<CoinAcceptorComponent>;
  let service: CoinAcceptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CoinAcceptorComponent],
    })
      .overrideTemplate(CoinAcceptorComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CoinAcceptorComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(CoinAcceptorService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.coinAcceptors?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
