import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICoinAcceptor } from '../coin-acceptor.model';
import { CoinAcceptorService } from '../service/coin-acceptor.service';
import { CoinAcceptorDeleteDialogComponent } from '../delete/coin-acceptor-delete-dialog.component';

@Component({
  selector: 'jhi-coin-acceptor',
  templateUrl: './coin-acceptor.component.html',
})
export class CoinAcceptorComponent implements OnInit {
  coinAcceptors?: ICoinAcceptor[];
  isLoading = false;

  constructor(protected coinAcceptorService: CoinAcceptorService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.coinAcceptorService.query().subscribe({
      next: (res: HttpResponse<ICoinAcceptor[]>) => {
        this.isLoading = false;
        this.coinAcceptors = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ICoinAcceptor): number {
    return item.id!;
  }

  delete(coinAcceptor: ICoinAcceptor): void {
    const modalRef = this.modalService.open(CoinAcceptorDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.coinAcceptor = coinAcceptor;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
