import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICoinAcceptor } from '../coin-acceptor.model';
import { CoinAcceptorService } from '../service/coin-acceptor.service';

@Component({
  templateUrl: './coin-acceptor-delete-dialog.component.html',
})
export class CoinAcceptorDeleteDialogComponent {
  coinAcceptor?: ICoinAcceptor;

  constructor(protected coinAcceptorService: CoinAcceptorService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.coinAcceptorService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
