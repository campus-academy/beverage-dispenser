import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICoinAcceptor, CoinAcceptor } from '../coin-acceptor.model';
import { CoinAcceptorService } from '../service/coin-acceptor.service';

@Injectable({ providedIn: 'root' })
export class CoinAcceptorRoutingResolveService implements Resolve<ICoinAcceptor> {
  constructor(protected service: CoinAcceptorService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICoinAcceptor> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((coinAcceptor: HttpResponse<CoinAcceptor>) => {
          if (coinAcceptor.body) {
            return of(coinAcceptor.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CoinAcceptor());
  }
}
