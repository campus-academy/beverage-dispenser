import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { ICoinAcceptor, CoinAcceptor } from '../coin-acceptor.model';
import { CoinAcceptorService } from '../service/coin-acceptor.service';

import { CoinAcceptorRoutingResolveService } from './coin-acceptor-routing-resolve.service';

describe('CoinAcceptor routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: CoinAcceptorRoutingResolveService;
  let service: CoinAcceptorService;
  let resultCoinAcceptor: ICoinAcceptor | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(CoinAcceptorRoutingResolveService);
    service = TestBed.inject(CoinAcceptorService);
    resultCoinAcceptor = undefined;
  });

  describe('resolve', () => {
    it('should return ICoinAcceptor returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCoinAcceptor = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCoinAcceptor).toEqual({ id: 123 });
    });

    it('should return new ICoinAcceptor if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCoinAcceptor = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultCoinAcceptor).toEqual(new CoinAcceptor());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as CoinAcceptor })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCoinAcceptor = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCoinAcceptor).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
