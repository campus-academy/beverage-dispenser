import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CoinAcceptorComponent } from '../list/coin-acceptor.component';
import { CoinAcceptorDetailComponent } from '../detail/coin-acceptor-detail.component';
import { CoinAcceptorUpdateComponent } from '../update/coin-acceptor-update.component';
import { CoinAcceptorRoutingResolveService } from './coin-acceptor-routing-resolve.service';

const coinAcceptorRoute: Routes = [
  {
    path: '',
    component: CoinAcceptorComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CoinAcceptorDetailComponent,
    resolve: {
      coinAcceptor: CoinAcceptorRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CoinAcceptorUpdateComponent,
    resolve: {
      coinAcceptor: CoinAcceptorRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CoinAcceptorUpdateComponent,
    resolve: {
      coinAcceptor: CoinAcceptorRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(coinAcceptorRoute)],
  exports: [RouterModule],
})
export class CoinAcceptorRoutingModule {}
