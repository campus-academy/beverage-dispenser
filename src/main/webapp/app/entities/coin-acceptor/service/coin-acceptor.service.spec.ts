import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICoinAcceptor, CoinAcceptor } from '../coin-acceptor.model';

import { CoinAcceptorService } from './coin-acceptor.service';

describe('CoinAcceptor Service', () => {
  let service: CoinAcceptorService;
  let httpMock: HttpTestingController;
  let elemDefault: ICoinAcceptor;
  let expectedResult: ICoinAcceptor | ICoinAcceptor[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CoinAcceptorService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      requestedCurrency: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a CoinAcceptor', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new CoinAcceptor()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a CoinAcceptor', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          requestedCurrency: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a CoinAcceptor', () => {
      const patchObject = Object.assign({}, new CoinAcceptor());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of CoinAcceptor', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          requestedCurrency: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a CoinAcceptor', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCoinAcceptorToCollectionIfMissing', () => {
      it('should add a CoinAcceptor to an empty array', () => {
        const coinAcceptor: ICoinAcceptor = { id: 123 };
        expectedResult = service.addCoinAcceptorToCollectionIfMissing([], coinAcceptor);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(coinAcceptor);
      });

      it('should not add a CoinAcceptor to an array that contains it', () => {
        const coinAcceptor: ICoinAcceptor = { id: 123 };
        const coinAcceptorCollection: ICoinAcceptor[] = [
          {
            ...coinAcceptor,
          },
          { id: 456 },
        ];
        expectedResult = service.addCoinAcceptorToCollectionIfMissing(coinAcceptorCollection, coinAcceptor);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a CoinAcceptor to an array that doesn't contain it", () => {
        const coinAcceptor: ICoinAcceptor = { id: 123 };
        const coinAcceptorCollection: ICoinAcceptor[] = [{ id: 456 }];
        expectedResult = service.addCoinAcceptorToCollectionIfMissing(coinAcceptorCollection, coinAcceptor);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(coinAcceptor);
      });

      it('should add only unique CoinAcceptor to an array', () => {
        const coinAcceptorArray: ICoinAcceptor[] = [{ id: 123 }, { id: 456 }, { id: 21791 }];
        const coinAcceptorCollection: ICoinAcceptor[] = [{ id: 123 }];
        expectedResult = service.addCoinAcceptorToCollectionIfMissing(coinAcceptorCollection, ...coinAcceptorArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const coinAcceptor: ICoinAcceptor = { id: 123 };
        const coinAcceptor2: ICoinAcceptor = { id: 456 };
        expectedResult = service.addCoinAcceptorToCollectionIfMissing([], coinAcceptor, coinAcceptor2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(coinAcceptor);
        expect(expectedResult).toContain(coinAcceptor2);
      });

      it('should accept null and undefined values', () => {
        const coinAcceptor: ICoinAcceptor = { id: 123 };
        expectedResult = service.addCoinAcceptorToCollectionIfMissing([], null, coinAcceptor, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(coinAcceptor);
      });

      it('should return initial array if no CoinAcceptor is added', () => {
        const coinAcceptorCollection: ICoinAcceptor[] = [{ id: 123 }];
        expectedResult = service.addCoinAcceptorToCollectionIfMissing(coinAcceptorCollection, undefined, null);
        expect(expectedResult).toEqual(coinAcceptorCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
