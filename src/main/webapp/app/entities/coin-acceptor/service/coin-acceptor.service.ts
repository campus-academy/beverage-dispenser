import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICoinAcceptor, getCoinAcceptorIdentifier } from '../coin-acceptor.model';

export type EntityResponseType = HttpResponse<ICoinAcceptor>;
export type EntityArrayResponseType = HttpResponse<ICoinAcceptor[]>;

@Injectable({ providedIn: 'root' })
export class CoinAcceptorService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/coin-acceptors');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(coinAcceptor: ICoinAcceptor): Observable<EntityResponseType> {
    return this.http.post<ICoinAcceptor>(this.resourceUrl, coinAcceptor, { observe: 'response' });
  }

  update(coinAcceptor: ICoinAcceptor): Observable<EntityResponseType> {
    return this.http.put<ICoinAcceptor>(`${this.resourceUrl}/${getCoinAcceptorIdentifier(coinAcceptor) as number}`, coinAcceptor, {
      observe: 'response',
    });
  }

  partialUpdate(coinAcceptor: ICoinAcceptor): Observable<EntityResponseType> {
    return this.http.patch<ICoinAcceptor>(`${this.resourceUrl}/${getCoinAcceptorIdentifier(coinAcceptor) as number}`, coinAcceptor, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICoinAcceptor>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICoinAcceptor[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCoinAcceptorToCollectionIfMissing(
    coinAcceptorCollection: ICoinAcceptor[],
    ...coinAcceptorsToCheck: (ICoinAcceptor | null | undefined)[]
  ): ICoinAcceptor[] {
    const coinAcceptors: ICoinAcceptor[] = coinAcceptorsToCheck.filter(isPresent);
    if (coinAcceptors.length > 0) {
      const coinAcceptorCollectionIdentifiers = coinAcceptorCollection.map(
        coinAcceptorItem => getCoinAcceptorIdentifier(coinAcceptorItem)!
      );
      const coinAcceptorsToAdd = coinAcceptors.filter(coinAcceptorItem => {
        const coinAcceptorIdentifier = getCoinAcceptorIdentifier(coinAcceptorItem);
        if (coinAcceptorIdentifier == null || coinAcceptorCollectionIdentifiers.includes(coinAcceptorIdentifier)) {
          return false;
        }
        coinAcceptorCollectionIdentifiers.push(coinAcceptorIdentifier);
        return true;
      });
      return [...coinAcceptorsToAdd, ...coinAcceptorCollection];
    }
    return coinAcceptorCollection;
  }

  sumChecker(idCoin: number): Promise<any> {
    return this.http.get(this.resourceUrl + `/1/coin/${idCoin}`, { observe: 'response' }).toPromise();
  }

  buyDrink(drinkId: number): Observable<EntityResponseType> {
    return this.http.post(this.resourceUrl, drinkId, { observe: 'response' });
  }
}
