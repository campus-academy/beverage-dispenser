export const EntityNavbarItems = [
  {
    name: 'Infrastructure',
    route: 'infrastructure',
    translationKey: 'global.menu.entities.infrastructure',
  },
  {
    name: 'Drink',
    route: 'drink',
    translationKey: 'global.menu.entities.drink',
  },
  {
    name: 'Ingredient',
    route: 'ingredient',
    translationKey: 'global.menu.entities.ingredient',
  },
  {
    name: 'VendingMachine',
    route: 'vending-machine',
    translationKey: 'global.menu.entities.vendingMachine',
  },
  {
    name: 'CoinAcceptor',
    route: 'coin-acceptor',
    translationKey: 'global.menu.entities.coinAcceptor',
  },
  {
    name: 'Coin',
    route: 'coin',
    translationKey: 'global.menu.entities.coin',
  },
  {
    name: 'History',
    route: 'history',
    translationKey: 'global.menu.entities.history',
  },
];
