import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'infrastructure',
        data: { pageTitle: 'beverageDispenserApp.infrastructure.home.title' },
        loadChildren: () => import('./infrastructure/infrastructure.module').then(m => m.InfrastructureModule),
      },
      {
        path: 'drink',
        data: { pageTitle: 'beverageDispenserApp.drink.home.title' },
        loadChildren: () => import('./drink/drink.module').then(m => m.DrinkModule),
      },
      {
        path: 'ingredient',
        data: { pageTitle: 'beverageDispenserApp.ingredient.home.title' },
        loadChildren: () => import('./ingredient/ingredient.module').then(m => m.IngredientModule),
      },
      {
        path: 'vending-machine',
        data: { pageTitle: 'beverageDispenserApp.vendingMachine.home.title' },
        loadChildren: () => import('./vending-machine/vending-machine.module').then(m => m.VendingMachineModule),
      },
      {
        path: 'coin-acceptor',
        data: { pageTitle: 'beverageDispenserApp.coinAcceptor.home.title' },
        loadChildren: () => import('./coin-acceptor/coin-acceptor.module').then(m => m.CoinAcceptorModule),
      },
      {
        path: 'coin',
        data: { pageTitle: 'beverageDispenserApp.coin.home.title' },
        loadChildren: () => import('./coin/coin.module').then(m => m.CoinModule),
      },
      {
        path: 'history',
        data: { pageTitle: 'beverageDispenserApp.history.home.title' },
        loadChildren: () => import('./history/history.module').then(m => m.HistoryModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
