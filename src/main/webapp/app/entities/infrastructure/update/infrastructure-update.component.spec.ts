import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { InfrastructureService } from '../service/infrastructure.service';
import { IInfrastructure, Infrastructure } from '../infrastructure.model';

import { InfrastructureUpdateComponent } from './infrastructure-update.component';

describe('Infrastructure Management Update Component', () => {
  let comp: InfrastructureUpdateComponent;
  let fixture: ComponentFixture<InfrastructureUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let infrastructureService: InfrastructureService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [InfrastructureUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(InfrastructureUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(InfrastructureUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    infrastructureService = TestBed.inject(InfrastructureService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const infrastructure: IInfrastructure = { id: 456 };

      activatedRoute.data = of({ infrastructure });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(infrastructure));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Infrastructure>>();
      const infrastructure = { id: 123 };
      jest.spyOn(infrastructureService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ infrastructure });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: infrastructure }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(infrastructureService.update).toHaveBeenCalledWith(infrastructure);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Infrastructure>>();
      const infrastructure = new Infrastructure();
      jest.spyOn(infrastructureService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ infrastructure });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: infrastructure }));
      saveSubject.complete();

      // THEN
      expect(infrastructureService.create).toHaveBeenCalledWith(infrastructure);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Infrastructure>>();
      const infrastructure = { id: 123 };
      jest.spyOn(infrastructureService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ infrastructure });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(infrastructureService.update).toHaveBeenCalledWith(infrastructure);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
