import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IInfrastructure, Infrastructure } from '../infrastructure.model';
import { InfrastructureService } from '../service/infrastructure.service';

@Component({
  selector: 'jhi-infrastructure-update',
  templateUrl: './infrastructure-update.component.html',
})
export class InfrastructureUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
  });

  constructor(
    protected infrastructureService: InfrastructureService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ infrastructure }) => {
      this.updateForm(infrastructure);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const infrastructure = this.createFromForm();
    if (infrastructure.id !== undefined) {
      this.subscribeToSaveResponse(this.infrastructureService.update(infrastructure));
    } else {
      this.subscribeToSaveResponse(this.infrastructureService.create(infrastructure));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInfrastructure>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(infrastructure: IInfrastructure): void {
    this.editForm.patchValue({
      id: infrastructure.id,
      name: infrastructure.name,
    });
  }

  protected createFromForm(): IInfrastructure {
    return {
      ...new Infrastructure(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }
}
