import { IVendingMachine } from 'app/entities/vending-machine/vending-machine.model';

export interface IInfrastructure {
  id?: number;
  name?: string | null;
  vendingMachines?: IVendingMachine[] | null;
}

export class Infrastructure implements IInfrastructure {
  constructor(public id?: number, public name?: string | null, public vendingMachines?: IVendingMachine[] | null) {}
}

export function getInfrastructureIdentifier(infrastructure: IInfrastructure): number | undefined {
  return infrastructure.id;
}
