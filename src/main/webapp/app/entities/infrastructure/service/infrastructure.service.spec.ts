import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IInfrastructure, Infrastructure } from '../infrastructure.model';

import { InfrastructureService } from './infrastructure.service';

describe('Infrastructure Service', () => {
  let service: InfrastructureService;
  let httpMock: HttpTestingController;
  let elemDefault: IInfrastructure;
  let expectedResult: IInfrastructure | IInfrastructure[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(InfrastructureService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Infrastructure', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Infrastructure()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Infrastructure', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Infrastructure', () => {
      const patchObject = Object.assign(
        {
          name: 'BBBBBB',
        },
        new Infrastructure()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Infrastructure', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Infrastructure', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addInfrastructureToCollectionIfMissing', () => {
      it('should add a Infrastructure to an empty array', () => {
        const infrastructure: IInfrastructure = { id: 123 };
        expectedResult = service.addInfrastructureToCollectionIfMissing([], infrastructure);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(infrastructure);
      });

      it('should not add a Infrastructure to an array that contains it', () => {
        const infrastructure: IInfrastructure = { id: 123 };
        const infrastructureCollection: IInfrastructure[] = [
          {
            ...infrastructure,
          },
          { id: 456 },
        ];
        expectedResult = service.addInfrastructureToCollectionIfMissing(infrastructureCollection, infrastructure);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Infrastructure to an array that doesn't contain it", () => {
        const infrastructure: IInfrastructure = { id: 123 };
        const infrastructureCollection: IInfrastructure[] = [{ id: 456 }];
        expectedResult = service.addInfrastructureToCollectionIfMissing(infrastructureCollection, infrastructure);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(infrastructure);
      });

      it('should add only unique Infrastructure to an array', () => {
        const infrastructureArray: IInfrastructure[] = [{ id: 123 }, { id: 456 }, { id: 27359 }];
        const infrastructureCollection: IInfrastructure[] = [{ id: 123 }];
        expectedResult = service.addInfrastructureToCollectionIfMissing(infrastructureCollection, ...infrastructureArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const infrastructure: IInfrastructure = { id: 123 };
        const infrastructure2: IInfrastructure = { id: 456 };
        expectedResult = service.addInfrastructureToCollectionIfMissing([], infrastructure, infrastructure2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(infrastructure);
        expect(expectedResult).toContain(infrastructure2);
      });

      it('should accept null and undefined values', () => {
        const infrastructure: IInfrastructure = { id: 123 };
        expectedResult = service.addInfrastructureToCollectionIfMissing([], null, infrastructure, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(infrastructure);
      });

      it('should return initial array if no Infrastructure is added', () => {
        const infrastructureCollection: IInfrastructure[] = [{ id: 123 }];
        expectedResult = service.addInfrastructureToCollectionIfMissing(infrastructureCollection, undefined, null);
        expect(expectedResult).toEqual(infrastructureCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
