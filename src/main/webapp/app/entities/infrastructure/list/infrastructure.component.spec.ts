import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { InfrastructureService } from '../service/infrastructure.service';

import { InfrastructureComponent } from './infrastructure.component';

describe('Infrastructure Management Component', () => {
  let comp: InfrastructureComponent;
  let fixture: ComponentFixture<InfrastructureComponent>;
  let service: InfrastructureService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [InfrastructureComponent],
    })
      .overrideTemplate(InfrastructureComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(InfrastructureComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(InfrastructureService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.infrastructures?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
