import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IInfrastructure } from '../infrastructure.model';
import { InfrastructureService } from '../service/infrastructure.service';
import { InfrastructureDeleteDialogComponent } from '../delete/infrastructure-delete-dialog.component';

@Component({
  selector: 'jhi-infrastructure',
  templateUrl: './infrastructure.component.html',
})
export class InfrastructureComponent implements OnInit {
  infrastructures?: IInfrastructure[];
  isLoading = false;

  constructor(protected infrastructureService: InfrastructureService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.infrastructureService.query().subscribe({
      next: (res: HttpResponse<IInfrastructure[]>) => {
        this.isLoading = false;
        this.infrastructures = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IInfrastructure): number {
    return item.id!;
  }

  delete(infrastructure: IInfrastructure): void {
    const modalRef = this.modalService.open(InfrastructureDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.infrastructure = infrastructure;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
