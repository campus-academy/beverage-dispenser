import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { InfrastructureComponent } from './list/infrastructure.component';
import { InfrastructureDetailComponent } from './detail/infrastructure-detail.component';
import { InfrastructureUpdateComponent } from './update/infrastructure-update.component';
import { InfrastructureDeleteDialogComponent } from './delete/infrastructure-delete-dialog.component';
import { InfrastructureRoutingModule } from './route/infrastructure-routing.module';

@NgModule({
  imports: [SharedModule, InfrastructureRoutingModule],
  declarations: [
    InfrastructureComponent,
    InfrastructureDetailComponent,
    InfrastructureUpdateComponent,
    InfrastructureDeleteDialogComponent,
  ],
  entryComponents: [InfrastructureDeleteDialogComponent],
})
export class InfrastructureModule {}
