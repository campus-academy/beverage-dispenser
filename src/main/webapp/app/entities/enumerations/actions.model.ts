export enum Actions {
  BUY = 'BUY',

  RESTOCK = 'RESTOCK',

  OTHER = 'OTHER',
}
