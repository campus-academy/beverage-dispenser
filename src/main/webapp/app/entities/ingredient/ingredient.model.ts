import { IDrink } from 'app/entities/drink/drink.model';

export interface IIngredient {
  id?: number;
  name?: string | null;
  stock?: number | null;
  drinks?: IDrink[] | null;
}

export class Ingredient implements IIngredient {
  constructor(public id?: number, public name?: string | null, public stock?: number | null, public drinks?: IDrink[] | null) {}
}

export function getIngredientIdentifier(ingredient: IIngredient): number | undefined {
  return ingredient.id;
}
