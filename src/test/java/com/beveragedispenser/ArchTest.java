package com.beveragedispenser;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.beveragedispenser");

        noClasses()
            .that()
            .resideInAnyPackage("com.beveragedispenser.service..")
            .or()
            .resideInAnyPackage("com.beveragedispenser.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..com.beveragedispenser.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
