package com.beveragedispenser.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.beveragedispenser.IntegrationTest;
import com.beveragedispenser.domain.VendingMachine;
import com.beveragedispenser.repository.VendingMachineRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link VendingMachineResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class VendingMachineResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SERIAL_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_SERIAL_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_BRAND = "AAAAAAAAAA";
    private static final String UPDATED_BRAND = "BBBBBBBBBB";

    private static final String DEFAULT_MODEL = "AAAAAAAAAA";
    private static final String UPDATED_MODEL = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/vending-machines";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private VendingMachineRepository vendingMachineRepository;

    @Mock
    private VendingMachineRepository vendingMachineRepositoryMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVendingMachineMockMvc;

    private VendingMachine vendingMachine;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VendingMachine createEntity(EntityManager em) {
        VendingMachine vendingMachine = new VendingMachine()
            .name(DEFAULT_NAME)
            .serialNumber(DEFAULT_SERIAL_NUMBER)
            .brand(DEFAULT_BRAND)
            .model(DEFAULT_MODEL);
        return vendingMachine;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VendingMachine createUpdatedEntity(EntityManager em) {
        VendingMachine vendingMachine = new VendingMachine()
            .name(UPDATED_NAME)
            .serialNumber(UPDATED_SERIAL_NUMBER)
            .brand(UPDATED_BRAND)
            .model(UPDATED_MODEL);
        return vendingMachine;
    }

    @BeforeEach
    public void initTest() {
        vendingMachine = createEntity(em);
    }

    @Test
    @Transactional
    void createVendingMachine() throws Exception {
        int databaseSizeBeforeCreate = vendingMachineRepository.findAll().size();
        // Create the VendingMachine
        restVendingMachineMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vendingMachine))
            )
            .andExpect(status().isCreated());

        // Validate the VendingMachine in the database
        List<VendingMachine> vendingMachineList = vendingMachineRepository.findAll();
        assertThat(vendingMachineList).hasSize(databaseSizeBeforeCreate + 1);
        VendingMachine testVendingMachine = vendingMachineList.get(vendingMachineList.size() - 1);
        assertThat(testVendingMachine.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testVendingMachine.getSerialNumber()).isEqualTo(DEFAULT_SERIAL_NUMBER);
        assertThat(testVendingMachine.getBrand()).isEqualTo(DEFAULT_BRAND);
        assertThat(testVendingMachine.getModel()).isEqualTo(DEFAULT_MODEL);
    }

    @Test
    @Transactional
    void createVendingMachineWithExistingId() throws Exception {
        // Create the VendingMachine with an existing ID
        vendingMachine.setId(1L);

        int databaseSizeBeforeCreate = vendingMachineRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restVendingMachineMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vendingMachine))
            )
            .andExpect(status().isBadRequest());

        // Validate the VendingMachine in the database
        List<VendingMachine> vendingMachineList = vendingMachineRepository.findAll();
        assertThat(vendingMachineList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllVendingMachines() throws Exception {
        // Initialize the database
        vendingMachineRepository.saveAndFlush(vendingMachine);

        // Get all the vendingMachineList
        restVendingMachineMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vendingMachine.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].serialNumber").value(hasItem(DEFAULT_SERIAL_NUMBER)))
            .andExpect(jsonPath("$.[*].brand").value(hasItem(DEFAULT_BRAND)))
            .andExpect(jsonPath("$.[*].model").value(hasItem(DEFAULT_MODEL)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllVendingMachinesWithEagerRelationshipsIsEnabled() throws Exception {
        when(vendingMachineRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restVendingMachineMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(vendingMachineRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllVendingMachinesWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(vendingMachineRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restVendingMachineMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(vendingMachineRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    void getVendingMachine() throws Exception {
        // Initialize the database
        vendingMachineRepository.saveAndFlush(vendingMachine);

        // Get the vendingMachine
        restVendingMachineMockMvc
            .perform(get(ENTITY_API_URL_ID, vendingMachine.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(vendingMachine.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.serialNumber").value(DEFAULT_SERIAL_NUMBER))
            .andExpect(jsonPath("$.brand").value(DEFAULT_BRAND))
            .andExpect(jsonPath("$.model").value(DEFAULT_MODEL));
    }

    @Test
    @Transactional
    void getNonExistingVendingMachine() throws Exception {
        // Get the vendingMachine
        restVendingMachineMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewVendingMachine() throws Exception {
        // Initialize the database
        vendingMachineRepository.saveAndFlush(vendingMachine);

        int databaseSizeBeforeUpdate = vendingMachineRepository.findAll().size();

        // Update the vendingMachine
        VendingMachine updatedVendingMachine = vendingMachineRepository.findById(vendingMachine.getId()).get();
        // Disconnect from session so that the updates on updatedVendingMachine are not directly saved in db
        em.detach(updatedVendingMachine);
        updatedVendingMachine.name(UPDATED_NAME).serialNumber(UPDATED_SERIAL_NUMBER).brand(UPDATED_BRAND).model(UPDATED_MODEL);

        restVendingMachineMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedVendingMachine.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedVendingMachine))
            )
            .andExpect(status().isOk());

        // Validate the VendingMachine in the database
        List<VendingMachine> vendingMachineList = vendingMachineRepository.findAll();
        assertThat(vendingMachineList).hasSize(databaseSizeBeforeUpdate);
        VendingMachine testVendingMachine = vendingMachineList.get(vendingMachineList.size() - 1);
        assertThat(testVendingMachine.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVendingMachine.getSerialNumber()).isEqualTo(UPDATED_SERIAL_NUMBER);
        assertThat(testVendingMachine.getBrand()).isEqualTo(UPDATED_BRAND);
        assertThat(testVendingMachine.getModel()).isEqualTo(UPDATED_MODEL);
    }

    @Test
    @Transactional
    void putNonExistingVendingMachine() throws Exception {
        int databaseSizeBeforeUpdate = vendingMachineRepository.findAll().size();
        vendingMachine.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVendingMachineMockMvc
            .perform(
                put(ENTITY_API_URL_ID, vendingMachine.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(vendingMachine))
            )
            .andExpect(status().isBadRequest());

        // Validate the VendingMachine in the database
        List<VendingMachine> vendingMachineList = vendingMachineRepository.findAll();
        assertThat(vendingMachineList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchVendingMachine() throws Exception {
        int databaseSizeBeforeUpdate = vendingMachineRepository.findAll().size();
        vendingMachine.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVendingMachineMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(vendingMachine))
            )
            .andExpect(status().isBadRequest());

        // Validate the VendingMachine in the database
        List<VendingMachine> vendingMachineList = vendingMachineRepository.findAll();
        assertThat(vendingMachineList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamVendingMachine() throws Exception {
        int databaseSizeBeforeUpdate = vendingMachineRepository.findAll().size();
        vendingMachine.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVendingMachineMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vendingMachine)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the VendingMachine in the database
        List<VendingMachine> vendingMachineList = vendingMachineRepository.findAll();
        assertThat(vendingMachineList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateVendingMachineWithPatch() throws Exception {
        // Initialize the database
        vendingMachineRepository.saveAndFlush(vendingMachine);

        int databaseSizeBeforeUpdate = vendingMachineRepository.findAll().size();

        // Update the vendingMachine using partial update
        VendingMachine partialUpdatedVendingMachine = new VendingMachine();
        partialUpdatedVendingMachine.setId(vendingMachine.getId());

        partialUpdatedVendingMachine.serialNumber(UPDATED_SERIAL_NUMBER).brand(UPDATED_BRAND);

        restVendingMachineMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedVendingMachine.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVendingMachine))
            )
            .andExpect(status().isOk());

        // Validate the VendingMachine in the database
        List<VendingMachine> vendingMachineList = vendingMachineRepository.findAll();
        assertThat(vendingMachineList).hasSize(databaseSizeBeforeUpdate);
        VendingMachine testVendingMachine = vendingMachineList.get(vendingMachineList.size() - 1);
        assertThat(testVendingMachine.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testVendingMachine.getSerialNumber()).isEqualTo(UPDATED_SERIAL_NUMBER);
        assertThat(testVendingMachine.getBrand()).isEqualTo(UPDATED_BRAND);
        assertThat(testVendingMachine.getModel()).isEqualTo(DEFAULT_MODEL);
    }

    @Test
    @Transactional
    void fullUpdateVendingMachineWithPatch() throws Exception {
        // Initialize the database
        vendingMachineRepository.saveAndFlush(vendingMachine);

        int databaseSizeBeforeUpdate = vendingMachineRepository.findAll().size();

        // Update the vendingMachine using partial update
        VendingMachine partialUpdatedVendingMachine = new VendingMachine();
        partialUpdatedVendingMachine.setId(vendingMachine.getId());

        partialUpdatedVendingMachine.name(UPDATED_NAME).serialNumber(UPDATED_SERIAL_NUMBER).brand(UPDATED_BRAND).model(UPDATED_MODEL);

        restVendingMachineMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedVendingMachine.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVendingMachine))
            )
            .andExpect(status().isOk());

        // Validate the VendingMachine in the database
        List<VendingMachine> vendingMachineList = vendingMachineRepository.findAll();
        assertThat(vendingMachineList).hasSize(databaseSizeBeforeUpdate);
        VendingMachine testVendingMachine = vendingMachineList.get(vendingMachineList.size() - 1);
        assertThat(testVendingMachine.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVendingMachine.getSerialNumber()).isEqualTo(UPDATED_SERIAL_NUMBER);
        assertThat(testVendingMachine.getBrand()).isEqualTo(UPDATED_BRAND);
        assertThat(testVendingMachine.getModel()).isEqualTo(UPDATED_MODEL);
    }

    @Test
    @Transactional
    void patchNonExistingVendingMachine() throws Exception {
        int databaseSizeBeforeUpdate = vendingMachineRepository.findAll().size();
        vendingMachine.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVendingMachineMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, vendingMachine.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(vendingMachine))
            )
            .andExpect(status().isBadRequest());

        // Validate the VendingMachine in the database
        List<VendingMachine> vendingMachineList = vendingMachineRepository.findAll();
        assertThat(vendingMachineList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchVendingMachine() throws Exception {
        int databaseSizeBeforeUpdate = vendingMachineRepository.findAll().size();
        vendingMachine.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVendingMachineMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(vendingMachine))
            )
            .andExpect(status().isBadRequest());

        // Validate the VendingMachine in the database
        List<VendingMachine> vendingMachineList = vendingMachineRepository.findAll();
        assertThat(vendingMachineList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamVendingMachine() throws Exception {
        int databaseSizeBeforeUpdate = vendingMachineRepository.findAll().size();
        vendingMachine.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVendingMachineMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(vendingMachine))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the VendingMachine in the database
        List<VendingMachine> vendingMachineList = vendingMachineRepository.findAll();
        assertThat(vendingMachineList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteVendingMachine() throws Exception {
        // Initialize the database
        vendingMachineRepository.saveAndFlush(vendingMachine);

        int databaseSizeBeforeDelete = vendingMachineRepository.findAll().size();

        // Delete the vendingMachine
        restVendingMachineMockMvc
            .perform(delete(ENTITY_API_URL_ID, vendingMachine.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<VendingMachine> vendingMachineList = vendingMachineRepository.findAll();
        assertThat(vendingMachineList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
