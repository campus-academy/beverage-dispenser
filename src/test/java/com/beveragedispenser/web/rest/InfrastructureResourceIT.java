package com.beveragedispenser.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.beveragedispenser.IntegrationTest;
import com.beveragedispenser.domain.Infrastructure;
import com.beveragedispenser.repository.InfrastructureRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link InfrastructureResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class InfrastructureResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/infrastructures";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private InfrastructureRepository infrastructureRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restInfrastructureMockMvc;

    private Infrastructure infrastructure;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Infrastructure createEntity(EntityManager em) {
        Infrastructure infrastructure = new Infrastructure().name(DEFAULT_NAME);
        return infrastructure;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Infrastructure createUpdatedEntity(EntityManager em) {
        Infrastructure infrastructure = new Infrastructure().name(UPDATED_NAME);
        return infrastructure;
    }

    @BeforeEach
    public void initTest() {
        infrastructure = createEntity(em);
    }

    @Test
    @Transactional
    void createInfrastructure() throws Exception {
        int databaseSizeBeforeCreate = infrastructureRepository.findAll().size();
        // Create the Infrastructure
        restInfrastructureMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(infrastructure))
            )
            .andExpect(status().isCreated());

        // Validate the Infrastructure in the database
        List<Infrastructure> infrastructureList = infrastructureRepository.findAll();
        assertThat(infrastructureList).hasSize(databaseSizeBeforeCreate + 1);
        Infrastructure testInfrastructure = infrastructureList.get(infrastructureList.size() - 1);
        assertThat(testInfrastructure.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void createInfrastructureWithExistingId() throws Exception {
        // Create the Infrastructure with an existing ID
        infrastructure.setId(1L);

        int databaseSizeBeforeCreate = infrastructureRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restInfrastructureMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(infrastructure))
            )
            .andExpect(status().isBadRequest());

        // Validate the Infrastructure in the database
        List<Infrastructure> infrastructureList = infrastructureRepository.findAll();
        assertThat(infrastructureList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllInfrastructures() throws Exception {
        // Initialize the database
        infrastructureRepository.saveAndFlush(infrastructure);

        // Get all the infrastructureList
        restInfrastructureMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(infrastructure.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getInfrastructure() throws Exception {
        // Initialize the database
        infrastructureRepository.saveAndFlush(infrastructure);

        // Get the infrastructure
        restInfrastructureMockMvc
            .perform(get(ENTITY_API_URL_ID, infrastructure.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(infrastructure.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getNonExistingInfrastructure() throws Exception {
        // Get the infrastructure
        restInfrastructureMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewInfrastructure() throws Exception {
        // Initialize the database
        infrastructureRepository.saveAndFlush(infrastructure);

        int databaseSizeBeforeUpdate = infrastructureRepository.findAll().size();

        // Update the infrastructure
        Infrastructure updatedInfrastructure = infrastructureRepository.findById(infrastructure.getId()).get();
        // Disconnect from session so that the updates on updatedInfrastructure are not directly saved in db
        em.detach(updatedInfrastructure);
        updatedInfrastructure.name(UPDATED_NAME);

        restInfrastructureMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedInfrastructure.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedInfrastructure))
            )
            .andExpect(status().isOk());

        // Validate the Infrastructure in the database
        List<Infrastructure> infrastructureList = infrastructureRepository.findAll();
        assertThat(infrastructureList).hasSize(databaseSizeBeforeUpdate);
        Infrastructure testInfrastructure = infrastructureList.get(infrastructureList.size() - 1);
        assertThat(testInfrastructure.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void putNonExistingInfrastructure() throws Exception {
        int databaseSizeBeforeUpdate = infrastructureRepository.findAll().size();
        infrastructure.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInfrastructureMockMvc
            .perform(
                put(ENTITY_API_URL_ID, infrastructure.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(infrastructure))
            )
            .andExpect(status().isBadRequest());

        // Validate the Infrastructure in the database
        List<Infrastructure> infrastructureList = infrastructureRepository.findAll();
        assertThat(infrastructureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchInfrastructure() throws Exception {
        int databaseSizeBeforeUpdate = infrastructureRepository.findAll().size();
        infrastructure.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInfrastructureMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(infrastructure))
            )
            .andExpect(status().isBadRequest());

        // Validate the Infrastructure in the database
        List<Infrastructure> infrastructureList = infrastructureRepository.findAll();
        assertThat(infrastructureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamInfrastructure() throws Exception {
        int databaseSizeBeforeUpdate = infrastructureRepository.findAll().size();
        infrastructure.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInfrastructureMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(infrastructure)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Infrastructure in the database
        List<Infrastructure> infrastructureList = infrastructureRepository.findAll();
        assertThat(infrastructureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateInfrastructureWithPatch() throws Exception {
        // Initialize the database
        infrastructureRepository.saveAndFlush(infrastructure);

        int databaseSizeBeforeUpdate = infrastructureRepository.findAll().size();

        // Update the infrastructure using partial update
        Infrastructure partialUpdatedInfrastructure = new Infrastructure();
        partialUpdatedInfrastructure.setId(infrastructure.getId());

        partialUpdatedInfrastructure.name(UPDATED_NAME);

        restInfrastructureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedInfrastructure.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedInfrastructure))
            )
            .andExpect(status().isOk());

        // Validate the Infrastructure in the database
        List<Infrastructure> infrastructureList = infrastructureRepository.findAll();
        assertThat(infrastructureList).hasSize(databaseSizeBeforeUpdate);
        Infrastructure testInfrastructure = infrastructureList.get(infrastructureList.size() - 1);
        assertThat(testInfrastructure.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void fullUpdateInfrastructureWithPatch() throws Exception {
        // Initialize the database
        infrastructureRepository.saveAndFlush(infrastructure);

        int databaseSizeBeforeUpdate = infrastructureRepository.findAll().size();

        // Update the infrastructure using partial update
        Infrastructure partialUpdatedInfrastructure = new Infrastructure();
        partialUpdatedInfrastructure.setId(infrastructure.getId());

        partialUpdatedInfrastructure.name(UPDATED_NAME);

        restInfrastructureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedInfrastructure.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedInfrastructure))
            )
            .andExpect(status().isOk());

        // Validate the Infrastructure in the database
        List<Infrastructure> infrastructureList = infrastructureRepository.findAll();
        assertThat(infrastructureList).hasSize(databaseSizeBeforeUpdate);
        Infrastructure testInfrastructure = infrastructureList.get(infrastructureList.size() - 1);
        assertThat(testInfrastructure.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingInfrastructure() throws Exception {
        int databaseSizeBeforeUpdate = infrastructureRepository.findAll().size();
        infrastructure.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInfrastructureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, infrastructure.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(infrastructure))
            )
            .andExpect(status().isBadRequest());

        // Validate the Infrastructure in the database
        List<Infrastructure> infrastructureList = infrastructureRepository.findAll();
        assertThat(infrastructureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchInfrastructure() throws Exception {
        int databaseSizeBeforeUpdate = infrastructureRepository.findAll().size();
        infrastructure.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInfrastructureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(infrastructure))
            )
            .andExpect(status().isBadRequest());

        // Validate the Infrastructure in the database
        List<Infrastructure> infrastructureList = infrastructureRepository.findAll();
        assertThat(infrastructureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamInfrastructure() throws Exception {
        int databaseSizeBeforeUpdate = infrastructureRepository.findAll().size();
        infrastructure.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInfrastructureMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(infrastructure))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Infrastructure in the database
        List<Infrastructure> infrastructureList = infrastructureRepository.findAll();
        assertThat(infrastructureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteInfrastructure() throws Exception {
        // Initialize the database
        infrastructureRepository.saveAndFlush(infrastructure);

        int databaseSizeBeforeDelete = infrastructureRepository.findAll().size();

        // Delete the infrastructure
        restInfrastructureMockMvc
            .perform(delete(ENTITY_API_URL_ID, infrastructure.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Infrastructure> infrastructureList = infrastructureRepository.findAll();
        assertThat(infrastructureList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
