package com.beveragedispenser.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.beveragedispenser.IntegrationTest;
import com.beveragedispenser.domain.CoinAcceptor;
import com.beveragedispenser.repository.CoinAcceptorRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CoinAcceptorResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class CoinAcceptorResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_REQUESTED_CURRENCY = 1;
    private static final Integer UPDATED_REQUESTED_CURRENCY = 2;

    private static final String ENTITY_API_URL = "/api/coin-acceptors";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CoinAcceptorRepository coinAcceptorRepository;

    @Mock
    private CoinAcceptorRepository coinAcceptorRepositoryMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCoinAcceptorMockMvc;

    private CoinAcceptor coinAcceptor;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CoinAcceptor createEntity(EntityManager em) {
        CoinAcceptor coinAcceptor = new CoinAcceptor().name(DEFAULT_NAME).requestedCurrency(DEFAULT_REQUESTED_CURRENCY);
        return coinAcceptor;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CoinAcceptor createUpdatedEntity(EntityManager em) {
        CoinAcceptor coinAcceptor = new CoinAcceptor().name(UPDATED_NAME).requestedCurrency(UPDATED_REQUESTED_CURRENCY);
        return coinAcceptor;
    }

    @BeforeEach
    public void initTest() {
        coinAcceptor = createEntity(em);
    }

    @Test
    @Transactional
    void createCoinAcceptor() throws Exception {
        int databaseSizeBeforeCreate = coinAcceptorRepository.findAll().size();
        // Create the CoinAcceptor
        restCoinAcceptorMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coinAcceptor)))
            .andExpect(status().isCreated());

        // Validate the CoinAcceptor in the database
        List<CoinAcceptor> coinAcceptorList = coinAcceptorRepository.findAll();
        assertThat(coinAcceptorList).hasSize(databaseSizeBeforeCreate + 1);
        CoinAcceptor testCoinAcceptor = coinAcceptorList.get(coinAcceptorList.size() - 1);
        assertThat(testCoinAcceptor.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCoinAcceptor.getRequestedCurrency()).isEqualTo(DEFAULT_REQUESTED_CURRENCY);
    }

    @Test
    @Transactional
    void createCoinAcceptorWithExistingId() throws Exception {
        // Create the CoinAcceptor with an existing ID
        coinAcceptor.setId(1L);

        int databaseSizeBeforeCreate = coinAcceptorRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCoinAcceptorMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coinAcceptor)))
            .andExpect(status().isBadRequest());

        // Validate the CoinAcceptor in the database
        List<CoinAcceptor> coinAcceptorList = coinAcceptorRepository.findAll();
        assertThat(coinAcceptorList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCoinAcceptors() throws Exception {
        // Initialize the database
        coinAcceptorRepository.saveAndFlush(coinAcceptor);

        // Get all the coinAcceptorList
        restCoinAcceptorMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(coinAcceptor.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].requestedCurrency").value(hasItem(DEFAULT_REQUESTED_CURRENCY)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllCoinAcceptorsWithEagerRelationshipsIsEnabled() throws Exception {
        when(coinAcceptorRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restCoinAcceptorMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(coinAcceptorRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllCoinAcceptorsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(coinAcceptorRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restCoinAcceptorMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(coinAcceptorRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    void getCoinAcceptor() throws Exception {
        // Initialize the database
        coinAcceptorRepository.saveAndFlush(coinAcceptor);

        // Get the coinAcceptor
        restCoinAcceptorMockMvc
            .perform(get(ENTITY_API_URL_ID, coinAcceptor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(coinAcceptor.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.requestedCurrency").value(DEFAULT_REQUESTED_CURRENCY));
    }

    @Test
    @Transactional
    void getNonExistingCoinAcceptor() throws Exception {
        // Get the coinAcceptor
        restCoinAcceptorMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCoinAcceptor() throws Exception {
        // Initialize the database
        coinAcceptorRepository.saveAndFlush(coinAcceptor);

        int databaseSizeBeforeUpdate = coinAcceptorRepository.findAll().size();

        // Update the coinAcceptor
        CoinAcceptor updatedCoinAcceptor = coinAcceptorRepository.findById(coinAcceptor.getId()).get();
        // Disconnect from session so that the updates on updatedCoinAcceptor are not directly saved in db
        em.detach(updatedCoinAcceptor);
        updatedCoinAcceptor.name(UPDATED_NAME).requestedCurrency(UPDATED_REQUESTED_CURRENCY);

        restCoinAcceptorMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCoinAcceptor.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCoinAcceptor))
            )
            .andExpect(status().isOk());

        // Validate the CoinAcceptor in the database
        List<CoinAcceptor> coinAcceptorList = coinAcceptorRepository.findAll();
        assertThat(coinAcceptorList).hasSize(databaseSizeBeforeUpdate);
        CoinAcceptor testCoinAcceptor = coinAcceptorList.get(coinAcceptorList.size() - 1);
        assertThat(testCoinAcceptor.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCoinAcceptor.getRequestedCurrency()).isEqualTo(UPDATED_REQUESTED_CURRENCY);
    }

    @Test
    @Transactional
    void putNonExistingCoinAcceptor() throws Exception {
        int databaseSizeBeforeUpdate = coinAcceptorRepository.findAll().size();
        coinAcceptor.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCoinAcceptorMockMvc
            .perform(
                put(ENTITY_API_URL_ID, coinAcceptor.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(coinAcceptor))
            )
            .andExpect(status().isBadRequest());

        // Validate the CoinAcceptor in the database
        List<CoinAcceptor> coinAcceptorList = coinAcceptorRepository.findAll();
        assertThat(coinAcceptorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCoinAcceptor() throws Exception {
        int databaseSizeBeforeUpdate = coinAcceptorRepository.findAll().size();
        coinAcceptor.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoinAcceptorMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(coinAcceptor))
            )
            .andExpect(status().isBadRequest());

        // Validate the CoinAcceptor in the database
        List<CoinAcceptor> coinAcceptorList = coinAcceptorRepository.findAll();
        assertThat(coinAcceptorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCoinAcceptor() throws Exception {
        int databaseSizeBeforeUpdate = coinAcceptorRepository.findAll().size();
        coinAcceptor.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoinAcceptorMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coinAcceptor)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the CoinAcceptor in the database
        List<CoinAcceptor> coinAcceptorList = coinAcceptorRepository.findAll();
        assertThat(coinAcceptorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCoinAcceptorWithPatch() throws Exception {
        // Initialize the database
        coinAcceptorRepository.saveAndFlush(coinAcceptor);

        int databaseSizeBeforeUpdate = coinAcceptorRepository.findAll().size();

        // Update the coinAcceptor using partial update
        CoinAcceptor partialUpdatedCoinAcceptor = new CoinAcceptor();
        partialUpdatedCoinAcceptor.setId(coinAcceptor.getId());

        partialUpdatedCoinAcceptor.name(UPDATED_NAME);

        restCoinAcceptorMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCoinAcceptor.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCoinAcceptor))
            )
            .andExpect(status().isOk());

        // Validate the CoinAcceptor in the database
        List<CoinAcceptor> coinAcceptorList = coinAcceptorRepository.findAll();
        assertThat(coinAcceptorList).hasSize(databaseSizeBeforeUpdate);
        CoinAcceptor testCoinAcceptor = coinAcceptorList.get(coinAcceptorList.size() - 1);
        assertThat(testCoinAcceptor.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCoinAcceptor.getRequestedCurrency()).isEqualTo(DEFAULT_REQUESTED_CURRENCY);
    }

    @Test
    @Transactional
    void fullUpdateCoinAcceptorWithPatch() throws Exception {
        // Initialize the database
        coinAcceptorRepository.saveAndFlush(coinAcceptor);

        int databaseSizeBeforeUpdate = coinAcceptorRepository.findAll().size();

        // Update the coinAcceptor using partial update
        CoinAcceptor partialUpdatedCoinAcceptor = new CoinAcceptor();
        partialUpdatedCoinAcceptor.setId(coinAcceptor.getId());

        partialUpdatedCoinAcceptor.name(UPDATED_NAME).requestedCurrency(UPDATED_REQUESTED_CURRENCY);

        restCoinAcceptorMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCoinAcceptor.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCoinAcceptor))
            )
            .andExpect(status().isOk());

        // Validate the CoinAcceptor in the database
        List<CoinAcceptor> coinAcceptorList = coinAcceptorRepository.findAll();
        assertThat(coinAcceptorList).hasSize(databaseSizeBeforeUpdate);
        CoinAcceptor testCoinAcceptor = coinAcceptorList.get(coinAcceptorList.size() - 1);
        assertThat(testCoinAcceptor.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCoinAcceptor.getRequestedCurrency()).isEqualTo(UPDATED_REQUESTED_CURRENCY);
    }

    @Test
    @Transactional
    void patchNonExistingCoinAcceptor() throws Exception {
        int databaseSizeBeforeUpdate = coinAcceptorRepository.findAll().size();
        coinAcceptor.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCoinAcceptorMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, coinAcceptor.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(coinAcceptor))
            )
            .andExpect(status().isBadRequest());

        // Validate the CoinAcceptor in the database
        List<CoinAcceptor> coinAcceptorList = coinAcceptorRepository.findAll();
        assertThat(coinAcceptorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCoinAcceptor() throws Exception {
        int databaseSizeBeforeUpdate = coinAcceptorRepository.findAll().size();
        coinAcceptor.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoinAcceptorMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(coinAcceptor))
            )
            .andExpect(status().isBadRequest());

        // Validate the CoinAcceptor in the database
        List<CoinAcceptor> coinAcceptorList = coinAcceptorRepository.findAll();
        assertThat(coinAcceptorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCoinAcceptor() throws Exception {
        int databaseSizeBeforeUpdate = coinAcceptorRepository.findAll().size();
        coinAcceptor.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoinAcceptorMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(coinAcceptor))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CoinAcceptor in the database
        List<CoinAcceptor> coinAcceptorList = coinAcceptorRepository.findAll();
        assertThat(coinAcceptorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCoinAcceptor() throws Exception {
        // Initialize the database
        coinAcceptorRepository.saveAndFlush(coinAcceptor);

        int databaseSizeBeforeDelete = coinAcceptorRepository.findAll().size();

        // Delete the coinAcceptor
        restCoinAcceptorMockMvc
            .perform(delete(ENTITY_API_URL_ID, coinAcceptor.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CoinAcceptor> coinAcceptorList = coinAcceptorRepository.findAll();
        assertThat(coinAcceptorList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
