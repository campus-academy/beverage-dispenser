package com.beveragedispenser.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.beveragedispenser.IntegrationTest;
import com.beveragedispenser.domain.Drink;
import com.beveragedispenser.repository.DrinkRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DrinkResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class DrinkResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_PRICE = 1;
    private static final Integer UPDATED_PRICE = 2;

    private static final String ENTITY_API_URL = "/api/drinks";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DrinkRepository drinkRepository;

    @Mock
    private DrinkRepository drinkRepositoryMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDrinkMockMvc;

    private Drink drink;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Drink createEntity(EntityManager em) {
        Drink drink = new Drink().name(DEFAULT_NAME).price(DEFAULT_PRICE);
        return drink;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Drink createUpdatedEntity(EntityManager em) {
        Drink drink = new Drink().name(UPDATED_NAME).price(UPDATED_PRICE);
        return drink;
    }

    @BeforeEach
    public void initTest() {
        drink = createEntity(em);
    }

    @Test
    @Transactional
    void createDrink() throws Exception {
        int databaseSizeBeforeCreate = drinkRepository.findAll().size();
        // Create the Drink
        restDrinkMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(drink)))
            .andExpect(status().isCreated());

        // Validate the Drink in the database
        List<Drink> drinkList = drinkRepository.findAll();
        assertThat(drinkList).hasSize(databaseSizeBeforeCreate + 1);
        Drink testDrink = drinkList.get(drinkList.size() - 1);
        assertThat(testDrink.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDrink.getPrice()).isEqualTo(DEFAULT_PRICE);
    }

    @Test
    @Transactional
    void createDrinkWithExistingId() throws Exception {
        // Create the Drink with an existing ID
        drink.setId(1L);

        int databaseSizeBeforeCreate = drinkRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDrinkMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(drink)))
            .andExpect(status().isBadRequest());

        // Validate the Drink in the database
        List<Drink> drinkList = drinkRepository.findAll();
        assertThat(drinkList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllDrinks() throws Exception {
        // Initialize the database
        drinkRepository.saveAndFlush(drink);

        // Get all the drinkList
        restDrinkMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(drink.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllDrinksWithEagerRelationshipsIsEnabled() throws Exception {
        when(drinkRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restDrinkMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(drinkRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllDrinksWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(drinkRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restDrinkMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(drinkRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    void getDrink() throws Exception {
        // Initialize the database
        drinkRepository.saveAndFlush(drink);

        // Get the drink
        restDrinkMockMvc
            .perform(get(ENTITY_API_URL_ID, drink.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(drink.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE));
    }

    @Test
    @Transactional
    void getNonExistingDrink() throws Exception {
        // Get the drink
        restDrinkMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewDrink() throws Exception {
        // Initialize the database
        drinkRepository.saveAndFlush(drink);

        int databaseSizeBeforeUpdate = drinkRepository.findAll().size();

        // Update the drink
        Drink updatedDrink = drinkRepository.findById(drink.getId()).get();
        // Disconnect from session so that the updates on updatedDrink are not directly saved in db
        em.detach(updatedDrink);
        updatedDrink.name(UPDATED_NAME).price(UPDATED_PRICE);

        restDrinkMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedDrink.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedDrink))
            )
            .andExpect(status().isOk());

        // Validate the Drink in the database
        List<Drink> drinkList = drinkRepository.findAll();
        assertThat(drinkList).hasSize(databaseSizeBeforeUpdate);
        Drink testDrink = drinkList.get(drinkList.size() - 1);
        assertThat(testDrink.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDrink.getPrice()).isEqualTo(UPDATED_PRICE);
    }

    @Test
    @Transactional
    void putNonExistingDrink() throws Exception {
        int databaseSizeBeforeUpdate = drinkRepository.findAll().size();
        drink.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDrinkMockMvc
            .perform(
                put(ENTITY_API_URL_ID, drink.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(drink))
            )
            .andExpect(status().isBadRequest());

        // Validate the Drink in the database
        List<Drink> drinkList = drinkRepository.findAll();
        assertThat(drinkList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDrink() throws Exception {
        int databaseSizeBeforeUpdate = drinkRepository.findAll().size();
        drink.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDrinkMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(drink))
            )
            .andExpect(status().isBadRequest());

        // Validate the Drink in the database
        List<Drink> drinkList = drinkRepository.findAll();
        assertThat(drinkList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDrink() throws Exception {
        int databaseSizeBeforeUpdate = drinkRepository.findAll().size();
        drink.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDrinkMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(drink)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Drink in the database
        List<Drink> drinkList = drinkRepository.findAll();
        assertThat(drinkList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDrinkWithPatch() throws Exception {
        // Initialize the database
        drinkRepository.saveAndFlush(drink);

        int databaseSizeBeforeUpdate = drinkRepository.findAll().size();

        // Update the drink using partial update
        Drink partialUpdatedDrink = new Drink();
        partialUpdatedDrink.setId(drink.getId());

        partialUpdatedDrink.name(UPDATED_NAME);

        restDrinkMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDrink.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDrink))
            )
            .andExpect(status().isOk());

        // Validate the Drink in the database
        List<Drink> drinkList = drinkRepository.findAll();
        assertThat(drinkList).hasSize(databaseSizeBeforeUpdate);
        Drink testDrink = drinkList.get(drinkList.size() - 1);
        assertThat(testDrink.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDrink.getPrice()).isEqualTo(DEFAULT_PRICE);
    }

    @Test
    @Transactional
    void fullUpdateDrinkWithPatch() throws Exception {
        // Initialize the database
        drinkRepository.saveAndFlush(drink);

        int databaseSizeBeforeUpdate = drinkRepository.findAll().size();

        // Update the drink using partial update
        Drink partialUpdatedDrink = new Drink();
        partialUpdatedDrink.setId(drink.getId());

        partialUpdatedDrink.name(UPDATED_NAME).price(UPDATED_PRICE);

        restDrinkMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDrink.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDrink))
            )
            .andExpect(status().isOk());

        // Validate the Drink in the database
        List<Drink> drinkList = drinkRepository.findAll();
        assertThat(drinkList).hasSize(databaseSizeBeforeUpdate);
        Drink testDrink = drinkList.get(drinkList.size() - 1);
        assertThat(testDrink.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDrink.getPrice()).isEqualTo(UPDATED_PRICE);
    }

    @Test
    @Transactional
    void patchNonExistingDrink() throws Exception {
        int databaseSizeBeforeUpdate = drinkRepository.findAll().size();
        drink.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDrinkMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, drink.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(drink))
            )
            .andExpect(status().isBadRequest());

        // Validate the Drink in the database
        List<Drink> drinkList = drinkRepository.findAll();
        assertThat(drinkList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDrink() throws Exception {
        int databaseSizeBeforeUpdate = drinkRepository.findAll().size();
        drink.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDrinkMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(drink))
            )
            .andExpect(status().isBadRequest());

        // Validate the Drink in the database
        List<Drink> drinkList = drinkRepository.findAll();
        assertThat(drinkList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDrink() throws Exception {
        int databaseSizeBeforeUpdate = drinkRepository.findAll().size();
        drink.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDrinkMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(drink)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Drink in the database
        List<Drink> drinkList = drinkRepository.findAll();
        assertThat(drinkList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDrink() throws Exception {
        // Initialize the database
        drinkRepository.saveAndFlush(drink);

        int databaseSizeBeforeDelete = drinkRepository.findAll().size();

        // Delete the drink
        restDrinkMockMvc
            .perform(delete(ENTITY_API_URL_ID, drink.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Drink> drinkList = drinkRepository.findAll();
        assertThat(drinkList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
