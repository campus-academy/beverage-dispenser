package com.beveragedispenser.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.beveragedispenser.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class InfrastructureTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Infrastructure.class);
        Infrastructure infrastructure1 = new Infrastructure();
        infrastructure1.setId(1L);
        Infrastructure infrastructure2 = new Infrastructure();
        infrastructure2.setId(infrastructure1.getId());
        assertThat(infrastructure1).isEqualTo(infrastructure2);
        infrastructure2.setId(2L);
        assertThat(infrastructure1).isNotEqualTo(infrastructure2);
        infrastructure1.setId(null);
        assertThat(infrastructure1).isNotEqualTo(infrastructure2);
    }
}
