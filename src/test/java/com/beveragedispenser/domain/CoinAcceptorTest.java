package com.beveragedispenser.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.beveragedispenser.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CoinAcceptorTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CoinAcceptor.class);
        CoinAcceptor coinAcceptor1 = new CoinAcceptor();
        coinAcceptor1.setId(1L);
        CoinAcceptor coinAcceptor2 = new CoinAcceptor();
        coinAcceptor2.setId(coinAcceptor1.getId());
        assertThat(coinAcceptor1).isEqualTo(coinAcceptor2);
        coinAcceptor2.setId(2L);
        assertThat(coinAcceptor1).isNotEqualTo(coinAcceptor2);
        coinAcceptor1.setId(null);
        assertThat(coinAcceptor1).isNotEqualTo(coinAcceptor2);
    }
}
