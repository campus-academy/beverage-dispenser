package com.beveragedispenser.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.beveragedispenser.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class VendingMachineTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VendingMachine.class);
        VendingMachine vendingMachine1 = new VendingMachine();
        vendingMachine1.setId(1L);
        VendingMachine vendingMachine2 = new VendingMachine();
        vendingMachine2.setId(vendingMachine1.getId());
        assertThat(vendingMachine1).isEqualTo(vendingMachine2);
        vendingMachine2.setId(2L);
        assertThat(vendingMachine1).isNotEqualTo(vendingMachine2);
        vendingMachine1.setId(null);
        assertThat(vendingMachine1).isNotEqualTo(vendingMachine2);
    }
}
